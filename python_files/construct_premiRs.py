#!/usr/bin/python
#*-* coding: utf-8 *-*

""" Construct mirbase pre-miRNAs. """

import sys
import getopt


# def get_mir_tss(infile):
#     """ Create a dictionnary linking pre-miRs to TSSs. """
#     tss = {}
#     with open(infile) as stream:
#         for line in stream:
#             spl = line.rstrip().split('\t')
#             premirs = spl[3].split(';')[2].split(',')
#             for premir in premirs:
#                 if premir in tss:
#                     sys.exit('microRNA {0} appears twice\n'.format(premir))
#                 tss[premir] = spl
#     return tss


def print_pre_mirnas(gff, out):
    """ Print the pre-miRNAs from mirBase. """
    with open(gff) as instream:
        pattern = 'miRNA_primary_transcript'
        with open(out, 'w') as outstream:
            for line in instream:
                if not line.startswith('#') and pattern in line:
                    spl = line.rstrip().split('\t')
                    premir = spl[-1].split('=')[-1]
                    chrom = spl[0]
                    start = int(spl[3]) - 30
                    end = int(spl[4]) + 30
                    strand = spl[6]                
                    # if primir in tss:
                    #     tss_mir = tss[primir]
                    #     if tss_mir[5] != spl[6] or tss_mir[0] != spl[0]:
                    #         print 'WARNING: diff strand or chr for {0}'.format(
                    #             primir)
                    #     mini = min(eval(tss_mir[1]), eval(spl[3]) - 1)
                    #     maxi = max(eval(tss_mir[2]), eval(spl[4]))
                    outstream.write(
                           '{0}\t{1}\t{2}\t{3}\t.\t{4}\n'.format(
                               chrom, start, end, strand, premir))


###############################################################################
#                               MAIN
###############################################################################
if __name__ == "__main__":
    usage = '''
    %s -m <mirbase gff3 file> -o <output file>
    ''' % (sys.argv[0])

    try:
        opts, args = getopt.getopt(sys.argv[1:], "m:o:h")
    except getopt.GetoptError:
        sys.exit(str(getopt.GetoptError) + usage)

    f5_tss_file = None
    mir_gff = None
    output = None
    for o, a in opts:
        if o == '-m':
            mir_gff = a
        elif o == '-o':
            output = a
        else:
            sys.exit(usage)
    if not(mir_gff and output):
        sys.exit(usage)

    # mir_tss = get_mir_tss(f5_tss_file)
    print_pre_mirnas(mir_gff, output)
