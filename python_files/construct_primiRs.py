#!/usr/bin/python
#*-* coding: utf-8 *-*

""" Construct primary miRNAs based on F5 TSSs and mirbase pre-miRNAs. """

import sys
import getopt


def get_mir_tss(infile):
    """ Create a dictionnary linking pre-miRs to TSSs. """
    tss = {}
    with open(infile) as stream:
        for line in stream:
            spl = line.rstrip().split('\t')
            premirs = spl[3].split(';')[2].split(',')
            for premir in premirs:
                if premir in tss:
                    sys.exit('microRNA {0} appears twice\n'.format(premir))
                tss[premir] = spl
    return tss


def print_pri_mirnas(tss, gff, out):
    """ Print the pri-miRNAs from miR TSSs to 3'end of pre-miRs. """
    with open(gff) as instream:
        pattern = 'miRNA_primary_transcript'
        with open(out, 'w') as outstream:
            for line in instream:
                if not line.startswith('#') and pattern in line:
                    spl = line.rstrip().split('\t')
                    primir = spl[-1].split('=')[-1]
                    if primir in tss:
                        tss_mir = tss[primir]
                        if tss_mir[5] != spl[6] or tss_mir[0] != spl[0]:
                            print 'WARNING: diff strand or chr for {0}'.format(
                                primir)
                        mini = min(eval(tss_mir[1]), eval(spl[3]) - 1)
                        maxi = max(eval(tss_mir[2]), eval(spl[4]))
                        outstream.write(
                            '{0}\t{1}\t{2}\t{3}\t.\t{4}\n'.format(
                                tss_mir[0], mini, maxi, primir, tss_mir[5]))


###############################################################################
#                               MAIN
###############################################################################
if __name__ == "__main__":
    usage = '''
    %s -t <F5 TSS file> -m <mirbase gff3 file> -o <output file>
        Note: the <F5 TSS file> is in bed format and constructed from the
            original file provided in Table S11 (as of 20160720).
    ''' % (sys.argv[0])

    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:m:o:h")
    except getopt.GetoptError:
        sys.exit(str(getopt.GetoptError) + usage)

    f5_tss_file = None
    mir_gff = None
    output = None
    for o, a in opts:
        if o == '-t':
            f5_tss_file = a
        elif o == '-m':
            mir_gff = a
        elif o == '-o':
            output = a
        else:
            sys.exit(usage)
    if not(f5_tss_file and mir_gff and output):
        sys.exit(usage)

    mir_tss = get_mir_tss(f5_tss_file)
    print_pri_mirnas(mir_tss, mir_gff, output)
