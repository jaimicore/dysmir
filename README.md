# README

[Cis-regulatory mutations associate with transcriptional and post-transcriptional deregulation of the gene regulatory program in cancers](https://www.biorxiv.org/content/10.1101/2020.06.25.170738v1)

This repository contains the code to reproduce the analysis and results from this publication.

You can read the manuscript in this [link](https://www.biorxiv.org/content/10.1101/2020.06.25.170738v1).

If you want to use the *dysmiR* pipeline to analyze your own data, please use the code from this repository: [dysmir_pipeline](https://bitbucket.org/jaimicore/dysmir_pipeline/src/master/).

