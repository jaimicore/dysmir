#############################
## Load required libraries ##
#############################
required.libraries <- c("circlize",
                        "ComplexHeatmap",
                        "dplyr",
                        "ggplot2",
                        "xseq")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}


###############################
## Cutoff defined by entropy ##
###############################

# # Wrapper for the java function
# get_threshold = function(int.arr, method){
#   rJava::.jinit("/storage/mathelierarea/processed/jamondra/Projects/dysmir/R_scripts/Utilities")
#   # rJava::.jinit("/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/R_scripts/Utilities")
#   rim = range(int.arr, na.rm = TRUE)
#   im.hist = as.vector(table(factor(int.arr, levels = rim[1]:rim[2])))
#   if (length(im.hist) > 1){
#     entropy.class = rJava::.jnew("MaxEntropy")
#     rJava::.jcall(entropy.class, "I", method, im.hist) + rim[1]
#   }else{
#     return(-1)
#   }
# }
# 
# ## The input is a numeric vector (class: vector)
# find.cutoff.by.entropy <- function(values){
# 
#   ## Chop the vector + round
#   values = round(values, digits = 1)
# 
#   ## Count the number of elements on each value
#   freq <- table(values)
# 
#   ## Map values to an array - needed by the thresholding function
#   map = data.frame(seq(1, length(freq), by = 1), as.numeric(names(freq)))
#   colnames(map) = c("index", "value")
# 
#   vec.arr = c()
#   ii = 1
#   #get only values for which we have an entry (chopped vector at max frequency)
#   for (i in 1:length(values)){
#     index = map$index[which(map$value == values[i])]
#     if (length(map) > 0){
#       vec.arr[ii] = index
#       ii = ii + 1
#     }
#   }
#   
#   # Compute the threshold based on maximum entropy
#   thresh = get_threshold(int.arr = vec.arr, method = "MaxEntropy")
#   thresh = map$value[which(map$index == thresh)]
# 
#   ## Return the threshold
#   return(thresh)
# }


#################################
## Generate randomized network ##
#################################
# 
# network <- subset(miR.mRNA.network, Weight_context_score_perc >= p & miR == mir)

## Given a dataframe representing a network with two columns: (1) Feature and (2) Target
## Return n networks with the same size and number of interaction for each feature.
## The interactions are randomly selected.
## User can control:
## 1) Target replacement (applied independently on any of the n networks)
## 2) No overlap between the input targets with the randomized targets
randomized.network <- function(network, n = 1, replace.element = FALSE, noovlp = TRUE){
  
  ## In case the network has more than two columns, keep only columns 1 and 2
  network <- network[,c(1:2)]
  
  ## Remove duplicated lines
  network <- unique.data.frame(network)
  
  ## Rename network columns
  colnames(network) <- c("Feature", "Target")
  
  ## Get the feature names
  features <- unique(unlist(network[,"Feature"]))
  nb.features <- length(features)
  
  ## Get the target names
  targets <<- unique(unlist(network[,"Target"]))
  nb.targets <- length(targets)
  
  ## For each feature, count the number of targets
  nb.targets.per.feature <<- table(network[,"Feature"])
  
  ## Split the network (data.frame) into a list
  ## Each entry is the feature name
  targets.per.feature <<- split(network, f = network$Feature)
  
  target.names.per.feature <<- sapply(targets.per.feature, function(df){
    unique(as.vector(unlist(df[,"Target"])))
  })
  
  
  ## The randomized networks are generated n times
  ## Default: 1
  randomized.network.df <- data.frame()
  for(i in 1:n){
    
    ## Iterate on each feature
    
    for(f in features){
      
      ## Generate one random network for a given feature
      random.network.f <- NULL
      random.network.f <- randomized.network.one.feature(f, noovlp = noovlp, replace.element = replace.element)
      
      ## Add the column with the network number
      random.network.f$Network_nb <- i
      
      ## Concat the network
      randomized.network.df <- rbind(randomized.network.df, random.network.f)
    }
  }
  
  return(randomized.network.df)
}

## This is the core function
## Generate one random network for only one feature (iterate to generate more networks)
## Number of targets is taken by previously defined objects
randomized.network.one.feature <- function(f, noovlp = TRUE, replace.element = FALSE){
  
  ## Get the number of targets for the query feature
  nb.targets.f <- nb.targets.per.feature[f]
  
  ## Get the target's names of the query feature
  targets.f <- as.vector(unlist(target.names.per.feature[f]))
  
  ## By default the input targets are not chosen in the randomized networks
  ## This paraneter can be set by the user
  if(noovlp == TRUE){
    
    targets.filtered <- targets[!targets %in% targets.f]
    
  } else {
    targets.filtered <- targets
  }
  
  ## Generate the random targets
  ## Note: replace parameter can be set by the user
  random.targets.f <- sample(targets.filtered, size = nb.targets.f, replace = replace.element)
  
  ## Create a data.frame with the results
  ## Add the feature + nb network colummns
  randomized <- data.frame(Feature = f, Target = random.targets.f)
  randomized$Feature <- as.vector(randomized$Feature)
  randomized$Target <- as.vector(randomized$Target)
  
  as.data.frame(randomized)
}


####################################
## Generate network (xseq format) ##
####################################
## Convert a data.frame network (mir, target, score) in a list of lists.
## Each sub-list is names as the miR and their content correspond to the association values of the target genes.
df.net.to.xseq <- function(network){

  ## Rename columns
  colnames(network) <- c("Gene", "Target_gene", "Assoc_score")

  ##
  net.list <- split(network, f = network$Gene)

  xseq.net <- lapply(net.list, function(l){

    scores <- l$Assoc_score
    names(scores) <- l$Target_gene

    l <- scores

  })

  return(xseq.net)

}


########################################
## Add columns to the premiRNAs names ##
########################################
## NOTE: the colnames in the miRNa expression table correspond to the premiRNAs, not to the
## mature miRNAS. This must be adapted because the networks are based on the mature miRNAs.
rename.add.columns.mature.mir <- function(tab, mir.dic){
  
  ## Transpose the matrix to apply the merge function using a column with the premiR names
  tab <- t(tab)
  tab <- as.data.frame(tab)
  
  ## Add this column to further use the merge function
  tab$premiR <- rownames(tab)
  
  ## Merge the expression table with the miR <-> premiR dictionary
  tab <- merge(tab, mir.dic, by = "premiR")
  
  mir.ids <- tab$mature_ID
  ## Remove the miR and premiR columns
  tab <- within.data.frame(tab, rm(premiR, miR, mature_ID))
  
  ## Rename the columns with the mature miRNA names.
  ## Trick: this is done after transposing, because colnames accepts repeated names, whilst rownames does not.
  tab <- data.frame(t(tab))
  colnames(tab) <- mir.ids
  
  return(tab)
}


###########################################################
## Convert the gene posterior probability in a dataframe 
# pg.to.df <- function(post.prob.tab, gene.name, mut.type){
  pg.to.df <- function(post.prob.tab, gene.name){
  
    # print(names(post.prob.tab$posterior.g))
    # print(gene.name %in% names(post.prob.tab$posterior.g))
    
    ## Clean names, mature::precursor (in case of required)
    gene.name <- gsub(gene.name, pattern = "^.+::", replacement = "")
    
    if(gene.name %in% names(post.prob.tab$posterior.g)){

      ## Get the specimen IDs of the mutated samples
      sample.names <- names(post.prob.tab$posterior.g[[gene.name]])
      dysreg.prob.names <- c("Down", "Neutral", "Up")
      
      ## Convert the list in a table
      dysreg.prob.tab <- data.frame(do.call(rbind, post.prob.tab$posterior.g[[gene.name]]))
      colnames(dysreg.prob.tab) <- dysreg.prob.names
      
      ## Add the miRNA + sample IDs
      dysreg.prob.tab$gene <- gene.name
      dysreg.prob.tab$specimen <- sample.names
      
      ## Calculate the max value and sign between the down (-) and Up (+) regulation column
      ## Column 1: Down
      ## Column 2: Neutral
      ## Column 3: Up
      dysreg.prob.parsed <- dysreg.prob.tab[,c(1,3,4,5)] %>% 
        group_by(gene, specimen) %>% 
        summarise(dysreg_prob = max(c(Down, Up)),
                  dysreg_sign = which.max(c(Down, Up)))
      dysreg.prob.parsed$dysreg_sign <- ifelse(dysreg.prob.parsed$dysreg_sign == 1, yes = -1, no = +1)
      
      dysreg.prob.parsed$dysreg_prob <- dysreg.prob.parsed$dysreg_prob * dysreg.prob.parsed$dysreg_sign
      dysreg.prob.parsed$dysreg_prob <- round(dysreg.prob.parsed$dysreg_prob, digits = 2)
      
      return(dysreg.prob.parsed[,c(1:3)])
      
    } else {
      return(NULL)
    }
  

}


###########################
## Dysregulation heatmap ##
###########################
## Given a gene name, this heatmap shows:
## 1) Heatmap cells: the dysregulation probabilities P(G) of all the connected genes
## 2) Column bar: sample posterior probability of the given gene
## 3) Row bar (Net weight): the connection strengt between the given gene and each target
## 4) Row bar (Expr weight): the relative expression value of the connected gene
dysreg.heatmap = function(gene,
                          posterior,
                          net,
                          cor.tab,
                          cis.prob,
                          target.expr,
                          dysreg.g.th = 0.5,
                          main="in_Cancer") {

  
  ## Extract the gene name
  gene = intersect(gene, names(posterior$posterior.g))
  
  if(is.null(unlist(posterior$posterior.g[gene]))){
    
    message("; ", gene, " - No significant dysregulated genes")
    connected.genes <- NULL
    nb.connected.genes <- NULL
    ht <- NULL
    dysreg.mat.filt.melted <- NULL
  } else {
    
    
    ## Get the max dysreg probability for each patient between down(-)/up(+) regulation.
    prob.g = lapply(posterior$posterior.g[gene],
                    function(z) sapply(z, function(z1)
                      sign(z1[,3] - z1[,1]) * pmax(z1[,3],  z1[,1])))
    
    
    ## Get F(D)
    post.d <- ConvertXseqOutput(posterior)
    colnames(post.d) <- c("sample", "hgnc_symbol", "prob_sample", "prob_gene")
    
    ## Get the significant samples for each significant gene
    post.d <- 
    post.d %>% 
      dplyr::filter(prob_gene >= 0.9 & prob_sample >= dysreg.g.th & hgnc_symbol == gene) %>% 
      dplyr::select(sample, hgnc_symbol)
    significant.samples <- unique(post.d$sample)
    
    id = sapply(prob.g, function(z) is.matrix(z))
    gene   = gene[id]
    prob.g = prob.g[id]
    legend.lab <- 20
    
    ## Function to calculate max aboslute
    absmax <- function(x) { x[which.max( abs(x) )]}
    
    ## Show the genes with an absolute max dyreg prob >= 0.5 in the significant samples
    high.dysreg.genes <- abs(apply(prob.g[[gene]][,significant.samples], 1, absmax)) >= dysreg.g.th
    
    ## Select the genes significantly dysregulated in two or more samples
    dysreg.mat.filt <- as.data.frame(prob.g[[gene]][high.dysreg.genes,])
    # stop(print(head(dysreg.mat.filt)))
    # dysreg.mat.filt <- dysreg.mat.filt[ apply(dysreg.mat.filt, 1, function(l){ length(which(abs(l) >= dysreg.g.th)) }) > 1 , ]
    # high.dysreg.genes <- rownames(dysreg.mat.filt)
    
    
    # if(sum(high.dysreg.genes) < 2){
    if(length(high.dysreg.genes) <= 10){
      message("; ", gene, " - No significant dysregulated genes")
      connected.genes <- NULL
      nb.connected.genes <- NULL
      ht <- NULL
      dysreg.mat.filt.melted <- NULL
      
    } else{
      
      ## Convert the actual expression value of the target genes in a Z-score
      ## Use function scale
      mut.samples <- colnames(prob.g[[gene]])
      
      # if(!is.null(target.expr)){
      #   target.expr.z <- apply(target.expr, 2, scale, center = TRUE, scale = TRUE)
      #   rownames(target.expr.z) <- rownames(target.expr)
      #   
      #   #   g.parsed <- gsub(gene, pattern = "::.+$",replacement = "")
      #   g.parsed <- gsub(gene, pattern = "^.+::",replacement = "")
      #   target.expr.z <- target.expr.z[mut.samples,g.parsed]
      #   
      #   target.expr.z.parsed <- ifelse(target.expr.z > 3, yes = 3, no = target.expr.z)
      #   target.expr.z.parsed <- ifelse(target.expr.z.parsed < -3, yes = -3, no = target.expr.z.parsed)
      # }
      
      #################################
      ## Prepare heatmap annotations ##
      #################################
      cis.post.prob.df <- NULL
      if(!is.null(cis.prob)){
        cis.post.prob.df <- pg.to.df(post.prob.tab = cis.prob,
                                     gene.name = gene)
      }
      cis.post.parsed.tab <- NULL
      
      
      if(!is.null(cis.post.prob.df) & !is.null(target.expr)){
        

        cis.post.parsed.tab <- cis.post.prob.df$dysreg_prob
        names(cis.post.parsed.tab) <- cis.post.prob.df$specimen
        
        cis.post.parsed.tab <- cis.post.parsed.tab[names(cis.post.parsed.tab) %in% names(posterior$posterior.f[[gene]][, 2])]
        
        ## Column annotation
        ## Sample posterior probability in the analyzed gene
        sample.post.prob.df <- data.frame(Sample_post_prob = posterior$posterior.f[[gene]][, 2],
                                          Cis_post_prob = cis.post.parsed.tab,
                                          Z_score = as.vector(target.expr.z.parsed))
        
        post_prob_color.palette <- list(Sample_post_prob = colorRamp2(c(0, 0.5, 1), c("#ffffcc", "#fd8d3c", "#b10026")),
                                        Cis_post_prob = colorRamp2(c(-1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1), rev(c("#d73027", "#f46d43", "#fdae61", "#fee090", "#ffffbf", "#e0f3f8", "#abd9e9", "#74add1", "#4575b4" ))),
                                        Z_score = colorRamp2(c(-3, -2, -1, 0, 1, 2, 3), rev(c("#b2182b", "#d6604d", "#f4a582", "#ffffff", "#bababa", "#878787", "#4d4d4d"))))
        
      } else{
        ## Column annotation
        ## Sample posterior probability in the analyzed gene
        sample.post.prob.df <- data.frame(Sample_post_prob = posterior$posterior.f[[gene]][, 2])
        
        post_prob_color.palette <- list(Sample_post_prob = colorRamp2(c(0, 0.5, 1), c("#edf8e9", "#74c476", "#005a32")))
        
      }
      
      samples.post.prob.bar <- HeatmapAnnotation(df = sample.post.prob.df, col = post_prob_color.palette, border = TRUE, show_annotation_name = F)
      
      
      ## Row annotations
      ## P(H): probability that the mutated samples are down/up regulated
      ph.vector <- apply(posterior$posterior.h[[gene]], 1, function(r){
        
        if(which.max(r) == 1){
          r[1] <- -r[1]
          
        } else {
          r[2]
        }
        
      })
      
      
      if(!is.null(cor.tab)){
        
        ## Row annotation: miRNA-target correlation Rho
        mirna.parsed.name <- gsub(gene, pattern = "::.+$", replacement = "")
        
        cor.tab$Spearman <- as.numeric(cor.tab$Spearman)
        cor.tab$Significance <- as.numeric(cor.tab$Significance)
        cor.tab$gene <- as.vector(cor.tab$gene)
        cor.tab$miRNA <- as.vector(cor.tab$miRNA)
        
        cor.tab$Significance <- ifelse(cor.tab$Significance >= 50, yes = 50, no = cor.tab$Significance)
        cor.tab$Significance <- ifelse(cor.tab$Significance < 0, yes = 0, no = cor.tab$Significance)
        cor.tab$Significance <- cor.tab$Significance * sign(cor.tab$Spearman)
        
        cor.tab.filt <- subset(cor.tab[,c("miRNA", "gene", "Spearman", "Significance")], miRNA == mirna.parsed.name & gene %in% rownames(dysreg.mat.filt)) %>%
          group_by(miRNA, gene) %>% 
          filter(Spearman == max(Spearman))
        
        cor.tab.filt <- as.data.frame(cor.tab.filt)
        rownames(cor.tab.filt) <- cor.tab.filt$gene
        
        
        row.annot.df <- data.frame(Prob_dysreg = ph.vector[rownames(dysreg.mat.filt)],
                                   Signif = round(cor.tab.filt[rownames(dysreg.mat.filt), "Significance"], digits = 2))
        row.annot.color.palette <- list(Prob_dysreg = colorRamp2(c(-1, -0.75, -0.5, 0, 0.5, 0.75, 1), c("#8073ac", "#b2abd2", "#f7f7f7", "#f7f7f7", "#f7f7f7", "#fdb863", "#e08214")),  ## Dysregulation color scale
                                        # Signif = colorRamp2(c(-100, -75, -50, -25, 0, 25, 50, 75, 100), rev(c("#d73027", "#f46d43", "#fdae61", "#fee08b", "#ffffbf", "#d9ef8b", "#a6d96a", "#66bd63", "#1a9850"))))
                                        Signif = colorRamp2(c(-50, -25, 0, 25, 50), rev(c("#d73027", "#f46d43", "#ffffbf", "#66bd63", "#1a9850"))))
        
        # Signif = colorRamp2(c(-200, -150, -100, -50, 0, 50, 100, 150, 200), c("#542788", "#8073ac", "#b2abd2", "#d8daeb", "#f7f7f7", "#fee0b6", "#fdb863", "#e08214", "#b35806")))           ## Spearman Rho color scale
        net.expr.row <- HeatmapAnnotation(df = row.annot.df, col = row.annot.color.palette,
                                          which = "row",
                                          width = unit(0.85, "cm"))
      } else{
        row.annot.df <- data.frame(Prob_dysreg = ph.vector)
        row.annot.color.palette <- list(Prob_dysreg = colorRamp2(c(-1, -0.75, -0.5, 0, 0.5, 0.75, 1), c("#8073ac", "#b2abd2", "#f7f7f7", "#f7f7f7", "#f7f7f7", "#fdb863", "#e08214")))    ## Up-regulation color scale
        net.expr.row <- HeatmapAnnotation(df = row.annot.df, col = row.annot.color.palette,
                                          which = "row",
                                          width = unit(0.85, "cm"))
      }
      
      ## Assign colors to the heatmap
      col_fun = colorRamp2(c(-1, 0, 1), c("#2166ac", "white", "#b2182b"))
      
      
      ##########################
      ## Draw complex heatmap ##
      ##########################
      ht = Heatmap(dysreg.mat.filt,
                   name = "Gene\nRegulatory\nStatus",
                   col = col_fun,
                   
                   ## Column annotations
                   top_annotation = samples.post.prob.bar,
                   
                   ## Heatmap title
                   column_title = paste(gene, main, sep=" "),
                   
                   ## Clustering
                   cluster_columns = function(m){ hclust(dist(m), method = "ward.D2")},
                   cluster_rows = function(m){ hclust(dist(m), method = "ward.D2")},
                   
                   # column_names_gp = gpar(cex = 0.5),
                   
                   ## Cell spacing
                   rect_gp = gpar(col = "white", lty = 1, lwd = 2, row = "white")
                   
      ) # + net.expr.row
      #draw(ht, padding = unit(c(4, 4, 5, 5), "mm"))
      # dev.off()
      ht <- draw(ht, heatmap_legend_side = "right", annotation_legend_side = "right", merge_legend = TRUE)
      
      
      ###### Get the number and names of the dysregulated genes in the network
      connected.genes <- names(which(abs(apply(prob.g[[gene]][,significant.samples], 1, absmax)) >= dysreg.g.th))
      nb.connected.genes <- length(connected.genes)
      
      
      # nb.connected.genes <- nrow(dysreg.mat.filt)
      # connected.genes <- rownames(dysreg.mat.filt)
      
      ## Melt the data frame and add the P(G) information
      dysreg.mat.filt.melted <- melt(as.matrix(dysreg.mat.filt))
      colnames(dysreg.mat.filt.melted) <- c("Target_gene_name", "Specimen", "Dysreg_prob_trans")
      dysreg.mat.filt.melted$Gene <- gene
      
      if(!is.null(cis.prob)){
        ## Add the P(G) cis information
        colnames(cis.post.prob.df) <- c("Gene_name", "Specimen", "Dysreg_prob_cis")
        dysreg.mat.filt.melted <- merge(dysreg.mat.filt.melted, cis.post.prob.df[,c(2,3)], by = "Specimen")
      }
      
      
      ## Add the correlation test information
      if(!is.null(cor.tab)){
        
        dysreg.mat.filt.melted <- merge(dysreg.mat.filt.melted, cor.tab.filt, by.x = "Target_gene_name", by.y = "gene")
        # dysreg.mat.filt.melted <- dysreg.mat.filt.melted[,c("Target_gene_name", "Gene", "Specimen", "Dysreg_prob_trans", "Dysreg_prob_cis", "Spearman", "Significance")]
        dysreg.mat.filt.melted <- dysreg.mat.filt.melted[,c("Target_gene_name", "Gene", "Specimen", "Dysreg_prob_trans", "Spearman", "Significance")]
      }
      
      
      # if(!is.null(target.expr)){
      #   ## Add the Z-score information
      #   zscore.df <- data.frame(Z_score_expr = target.expr.z)
      #   zscore.df$Specimen <- rownames(zscore.df)
      #   dysreg.mat.filt.melted <- merge(dysreg.mat.filt.melted, zscore.df, by = "Specimen")
      #   
      #   if(grepl(gene, pattern = "hsa-mir")){
      #     dysreg.mat.filt.melted <- dysreg.mat.filt.melted[,c("Specimen", "Target_gene_name", "Dysreg_prob_trans", "Gene", "Spearman", "Significance", "Z_score_expr")]
      #   }
      # }
    }
  }
  
  return(list(prob.g = prob.g,
              legend = legend,
              heat = ht,
              nb_connected_genes = nb.connected.genes,
              connected_genes =  connected.genes,
              summary_tab = dysreg.mat.filt.melted))
}
                

###########################################################
## Return a set of colors for the distinct mutation types
## The idea is to let this color fixed for the plots generated in distinct scripts
assign.colors <- function(data.type){
  
  if(data.type == "premiRNA"){
    
    assigned.colors <- c("#253494", "#7a0177", "#bd0026")
    names(assigned.colors) <- c("TFBS", "premiRNA_+_TFBS", "premiRNA")
    
  } else if(data.type == "PCG"){
    
    assigned.colors <- c("#253494", "#f16913", "#006837", "#a6761d", "#e6ab02")
    names(assigned.colors) <- c("TFBS", "LoF_+_TFBS", "LoF", "WXS", "WXS_+_TFBS")
    
  } else if(data.type == "PCG_miRNA"){
    
    assigned.colors <- c("#253494", "#7a0177", "#f16913", "#006837", "#a6761d", "#e6ab02")
    names(assigned.colors) <- c("TFBS", "premiRNA_+_TFBS", "LoF_+_TFBS", "LoF", "WXS", "WXS_+_TFBS")
    
  } else if(data.type == "Mutations"){
    
    assigned.colors <- c("#e41a1c", "#377eb8")
    names(assigned.colors) <- c("Mutated", "Non-mutated")

  } else if(data.type == "Cohorts"){
    
    assigned.colors <- c("#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d")
    names(assigned.colors) <- c("BRCA-US", "LIHC-US", "UCEC-US", "HNSC-US", "STAD-US", "LUAD-US", "LUSC-US")
    
  } else if(data.type == "Gene_types"){
    
    assigned.colors <- c("#a6761d", "#bd0026")
    names(assigned.colors) <- c("PCG", "miRNA")
    
  } else {
    assigned.colors <- NULL
  }
  
  return(assigned.colors)
  
}


## custom blank theme ggplot
blank_theme <- theme_minimal() +
  theme(
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    panel.border = element_blank(),
    panel.grid=element_blank(),
    axis.ticks = element_blank(),
    plot.title=element_text(size=14, face="bold")
  )

## Capitalize first letter
firstup <- function(x) {
  substr(x, 1, 1) <- toupper(substr(x, 1, 1))
  x
}



xseq.to.df.network <- function(network.filt) {
  
  i <- 0
  df.all.nets <- lapply(network.filt, function(l){
    
    i <<- i + 1
    
    gene <- names(network.filt[i])
    
    
    df.net <- data.frame(l)
    df.net$G2 <- rownames(df.net)
    df.net$G1 <- gene
    rownames(df.net) <- NULL
    
    df.net %>% 
      dplyr::rename("Assoc_score" = l) %>% 
      dplyr::select(c("G1", "G2", "Assoc_score"))
    
    
  })
  df.all.nets <- do.call(rbind, df.all.nets)
  rownames(df.all.nets) <- NULL
  
  return(df.all.nets)
}


# ## Iterate over the miRNAs
# for(mir in selected.miRs[1:20]){
#   
#   message(" ")
#   message("Calculating correlations: ", mir)
#   
#   ## Initialize data.frame to stor results
#   all.miR.target.corr <- data.frame()
#   
#   ## Iterate over the percentiles
#   mir.cor.by.perc <- data.frame()
#   for(p in percentiles){
#     
#     ## Filter the network by weighted predictions
#     miR.mRNA.network.filtered <- subset(miR.mRNA.network, Weight_context_score_perc >= p)
#     
#     ## Calculate the corr
#     cor.mir.mrna <- correlation.miR.mRNAs(mir, miR.mRNA.network.filtered)
#     cor.mir.mrna$Percentile <- p
#     mir.cor.by.perc <- rbind(mir.cor.by.perc, cor.mir.mrna)
#     
#     ##
#     
#   }
#   
#   ## Cor values to absolute values
#   mir.cor.by.perc$value_abs <- abs(mir.cor.by.perc$value)
#   
#   ## Summarize the results over the percentiles
#   mir.corr.per.summary <- mir.cor.by.perc %>%
#     group_by(Percentile, Var2) %>%
#     dplyr::summarize(Mean = mean(value_abs, na.rm=TRUE), Median = median(value_abs, na.rm=TRUE), N = length(value_abs >= median(value_abs, na.rm=TRUE)))
#   
#   ## Select the line with the highest median
#   selected.mir.corr.per <- mir.corr.per.summary[which(mir.corr.per.summary$Median == max(mir.corr.per.summary$Median)), ]
#   colnames(selected.mir.corr.per) <- c("Percentile", "Cor_type", "Mean", "Median", "N")
#   selected.mir.corr.per$miRNA <- mir
#   
#   ## In case that the same corr value is obtained more than one percentile, take the lowest percentile
#   ## Note: only one percentile value should be selected.
#   if(nrow(selected.mir.corr.per) > 1){
#     selected.mir.corr.per <- selected.mir.corr.per[which(selected.mir.corr.per$Percentile == min(selected.mir.corr.per$Percentile)),]
#   }
#   
#   ## Save the summarized results
#   mir.distrib.info.tab <- rbind(mir.distrib.info.tab, as.data.frame(selected.mir.corr.per))
#   
#   
#   ## Select the correlation values by the selected Score Percentile
#   selected.percentile <- selected.mir.corr.per$Percentile
#   mir.corr <- subset(mir.cor.by.perc, Percentile == selected.percentile)[,1:6] # c("Gene_name", "Correlation_type", "Cor", "miR", "Data_type", "Neg_ctrl_nb")
#   # mir.corr <- plot.correlation.miR.mRNAs(mir, miR.mRNA.network.filtered)
#   
#   ## Generate the filtered network (required to generate negative control)
#   mir.mra.netowrk.filtered <- unique.data.frame(mir.corr[,c("miR", "Var1")])
#   colnames(mir.mra.netowrk.filtered) <- c("miR", "Target_gene")
#   
#   ## Calculate the correlations in neg control
#   mir.corr.neg.ctrl <- correlation.miR.mRNAs.neg.ctrl(mir, mir.mra.netowrk.filtered, n)
#   
#   
#   ## Concatenate the results
#   all.miR.target.corr <- rbind(all.miR.target.corr , mir.corr)
#   all.miR.target.corr <- rbind(all.miR.target.corr , mir.corr.neg.ctrl)
#   
#   
#   ## Rename the columns
#   colnames(all.miR.target.corr) <- c("Gene_name", "Correlation_type", "Cor", "miR", "Data_type", "Neg_ctrl_nb")
#   all.miR.target.corr$Neg_ctrl_nb <- as.factor(all.miR.target.corr$Neg_ctrl_nb)
#   all.miR.target.corr$Cor_abs <- abs(all.miR.target.corr$Cor)
#   
#   
#   ## Create subtitle for the plots
#   subtitle.string <- paste0("Selected Weight_context_score_percentile ", selected.percentile, " :: Number of target genes in the network: ", nrow(mir.mra.netowrk.filtered))
#   
#   ## Iterate over the Correlation types
#   density.cor.plots <- list()
#   for(cor.coef in c("Pearson", "Spearman")){
#     
#     ## Select those entries corresponding to the query corr type
#     cor.table.coef <- subset(all.miR.target.corr, Correlation_type == cor.coef )
#     
#     ## Generate the alpha values manually: green for the miRNA-mRNA values (in the network)
#     ## and gray for the negative control
#     alpha.val <- ifelse(cor.table.coef$Neg_ctrl_nb == 0, yes = 1, no = 0.05)
#     
#     ## Separate the table in miRNa and Random data
#     cor.table.coef.miRNA <- subset(cor.table.coef, Data_type == "miRNA")
#     cor.table.coef.Random <- subset(cor.table.coef, Data_type == "Random")
#     
#     
#     ## Calculate the median + mean + quantiles
#     distrib.median.complete.miRNA <- round(median(cor.table.coef.miRNA$Cor), digits = 3)
#     distrib.median.absolute.miRNA <- round(median(cor.table.coef.miRNA$Cor_abs), digits = 3)
#     distrib.median.complete.Random <- round(median(cor.table.coef.Random$Cor), digits = 3)
#     distrib.median.absolute.Random <- round(median(cor.table.coef.Random$Cor_abs), digits = 3)
#     
#     
#     distrib.mean.complete.miRNA <- round(mean(cor.table.coef.miRNA$Cor), digits = 3)
#     distrib.mean.absolute.miRNA <- round(mean(cor.table.coef.miRNA$Cor_abs), digits = 3)
#     distrib.mean.complete.Random <- round(mean(cor.table.coef.Random$Cor), digits = 3)
#     distrib.mean.absolute.Random <- round(mean(cor.table.coef.Random$Cor_abs), digits = 3)
#     
#     distrib.66.quantile.absolute.miRNA <- round(quantile(cor.table.coef.miRNA$Cor_abs, 2/3), digits = 3)
#     distrib.66.quantile.absolute.Random <- round(quantile(cor.table.coef.Random$Cor_abs, 2/3), digits = 3)
#     
#     ## Plot arguments
#     line.width <- 0.75
#     ann.font.size <- 5
#     
#     color.key <- list()
#     color.key[["Mean_miRNA"]] <- "#a50f15"
#     color.key[["Median_miRNA"]] <- "#08519c"
#     color.key[["66_quantile_miRNA"]] <- "#006d2c"
#     
#     color.key[["Mean_Random"]] <- "#fc9272"
#     color.key[["Median_Random"]] <- "#9ecae1"
#     color.key[["66_quantile_Random"]] <- "#74c476"
#     
#     ## Density plot
#     density.cor.plots[[cor.coef]][["Complete"]] <- ggplot(data = cor.table.coef, aes(x=Cor, colour = Data_type, group = Neg_ctrl_nb, fill = Data_type)) +
#       geom_density(aes(alpha = alpha.val)) +
#       xlim(c(-1, 1)) +
#       ylim(c(0, 7)) +
#       scale_fill_manual(values = c("#00441b", rep("#bdbdbd", times = n))) +
#       scale_colour_manual(values = c("#00441b", "#bdbdbd")) +
#       labs(title = paste0("Distribution of cor values (", mir, " vs mRNA)"), x = cor.coef, y ="Density", subtitle = subtitle.string) +
#       guides(colour = "none", alpha = "none") +
#       # geom_vline(xintercept = distrib.median.complete.miRNA, size = line.width, colour = "#FF3721", linetype = "dashed", show.legend = TRUE) +
#       geom_vline(xintercept = distrib.mean.complete.miRNA, size = line.width, colour = color.key[["Mean_miRNA"]], linetype = "dashed", show.legend = TRUE) +
#       geom_vline(xintercept = distrib.median.complete.Random, size = line.width, colour = color.key[["Mean_Random"]], linetype = "dashed", show.legend = TRUE) +
#       theme_bw() +
#       theme(text=element_text(size = 12)) +
#       annotate("text", x = 0.6, y = 5, label = paste0("Mean : ", distrib.mean.complete.miRNA), color = color.key[["Mean_miRNA"]], size = ann.font.size, hjust = 0) + 
#       annotate("text", x = 0.6, y = 4.7, label = paste0("Mean (Random) : ", distrib.median.complete.Random), color = color.key[["Mean_Random"]], size = ann.font.size, hjust = 0)
#     
#     
#     density.cor.plots[[cor.coef]][["Absolute"]] <- ggplot(data = cor.table.coef, aes(x=Cor_abs, colour = Data_type, group = Neg_ctrl_nb, fill = Data_type)) +
#       geom_density(aes(alpha = alpha.val)) +
#       xlim(c(0, 1)) +
#       ylim(c(0, 7)) +
#       scale_fill_manual(values = c("#00441b", rep("#bdbdbd", times = n))) +
#       scale_colour_manual(values = c("#00441b", "#bdbdbd")) +
#       labs(title = paste0("Distribution of absolute cor values (", mir, " vs mRNA)"), x = paste("Absolute ", cor.coef), y ="Density", subtitle = subtitle.string) +
#       guides(colour = "none", alpha = "none") +
#       geom_vline(xintercept = distrib.mean.absolute.miRNA, size = line.width, colour = color.key[["Mean_miRNA"]], linetype = "dashed") +
#       geom_vline(xintercept = distrib.mean.absolute.Random, size = line.width, colour = color.key[["Mean_Random"]], linetype = "dashed") +
#       geom_vline(xintercept = distrib.66.quantile.absolute.miRNA, size = line.width, colour = color.key[["66_quantile_miRNA"]], linetype = "dashed") +
#       geom_vline(xintercept = distrib.66.quantile.absolute.Random, size = line.width, colour = color.key[["66_quantile_Random"]], linetype = "dashed") +
#       # geom_vline(xintercept = distrib.median.absolute.miRNA, size = line.width, colour = color.key[["Median_miRNA"]], linetype = "dashed") +
#       # geom_vline(xintercept = distrib.median.absolute.Random, size = line.width, colour = color.key[["Median_Random"]], linetype = "dashed") +
#       theme_bw() +
#       theme(text=element_text(size = 12)) +
#       annotate("text", x = 0.6, y = 5, label= paste0("Mean : ", distrib.mean.absolute.miRNA), color = color.key[["Mean_miRNA"]], size = ann.font.size, hjust = 0) + 
#       annotate("text", x = 0.6, y = 4.7, label= paste0("Mean (Random): ", distrib.mean.absolute.Random), color = color.key[["Mean_Random"]], size = ann.font.size, hjust = 0) +
#       annotate("text", x = 0.6, y = 4.4, label= paste0("66 percentile : ", distrib.66.quantile.absolute.miRNA), color = color.key[["66_quantile_miRNA"]], size = ann.font.size, hjust = 0) +
#       annotate("text", x = 0.6, y = 4.1, label= paste0("66 percentile (Random): ", distrib.66.quantile.absolute.Random), color = color.key[["66_quantile_Random"]], size = ann.font.size, hjust = 0)
#     # annotate("text", x = 0.75, y = 4.75, label = paste0("Median : ", distrib.median.absolute.miRNA), color = color.key[["Median_miRNA"]], size = ann.font.size, hjust = 0 ) +
#     # annotate("text", x = 0.75, y = 4, label = paste0("Median (Random): ", distrib.median.absolute.Random), color = color.key[["Median_Random"]], size = ann.font.size, hjust = 0 ) +
#     
#     
#   }
#   
#   ## Four plots in the same page
#   ggarrange(density.cor.plots[["Pearson"]][["Complete"]], density.cor.plots[["Pearson"]][["Absolute"]], density.cor.plots[["Spearman"]][["Complete"]], density.cor.plots[["Spearman"]][["Absolute"]],
#             heights = c(1),
#             widths = c(0.5),
#             ncol = 2,
#             nrow = 2,
#             align = "hv",
#             common.legend = TRUE,
#             legend = "bottom" )
#   
#   ## Export correlation plot
#   # miRNA.RNA.corr.plot <- file.path(folders.to.create[["plots"]], paste0(mir, "_vs_target_mRNA_corr_values.pdf"))
#   miRNA.RNA.corr.plot <- paste0("/home/jamondra/Downloads/miR_test/", mir, "_vs_target_mRNA_corr_values.pdf")
#   message("; Exporting correlation plot: ", mir, " vs target genes mRNA expression: ", miRNA.RNA.corr.plot)
#   ggsave(miRNA.RNA.corr.plot, device = "pdf", width = 25, height = 25, limitsize = FALSE)
#   
#   miRNA.RNA.corr.plot <- paste0("/home/jamondra/Downloads/miR_test/", mir, "_vs_target_mRNA_corr_values.jpeg")
#   message("; Exporting correlation plot: ", mir, " vs target genes mRNA expression: ", miRNA.RNA.corr.plot)
#   ggsave(miRNA.RNA.corr.plot, device = "jpeg", width = 25, height = 25, limitsize = FALSE)
#   
# }


# bash ../bin/create_latex_logos.sh -i ChIP-atlas_results/dm3/ERX032305_S2_Lint-1/central_enrichment/selected_motif/ERX032305_S2_Lint-1.501bp.fa.sites.centrimo.best.TF_associated -o ChIP-atlas_results/dm3/ERX032305_S2_Lint-1/central_enrichment/selected_motif/ERX032305_S2_Lin -1.501bp.fa.sites.centrimo.best.TF_associated.tex
# bash ../bin/create_latex_logos.sh -i ChIP-atlas_results/dm3/SRX1794233_S2_trx/central_enrichment/selected_motif/SRX1794233_S2_trx.501bp.fa.sites.centrimo.best.TF_associated -o ChIP-atlas_results/dm3/SRX1794233_S2_trx/central_enrichment/selected_motif/SRX1794233_S2_trx.501bp.fa.sites.centrimo.best.TF_associated.tex