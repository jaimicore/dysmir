#####################
## Load R packages ##
#####################
required.libraries <- c("data.table",
                        "dplyr",
                        "ggplot2",
                        "ggpubr",
                        "gridExtra",
                        "xseq")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    library(lib, character.only=TRUE)
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message("; Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("results.dir")) {
  stop("Missing mandatory argument (Results folder): results.dir ")
} else if (!exists("mut.tab.file")) {
  stop("Missing mandatory argument (Mutation table): mut.tab.file ")
} else if (!exists("expr.tab.file")) {
  stop("Missing mandatory argument (miR expression table): expr.tab.file  ")
} 

# else if (!exists("miR.premiR.tab.file")) {
#   stop("Missing mandatory argument (miR <-> pre-miR association table): miR.premiR.tab.file  ")
# }


## Set non-mandatory variables (only if they were not declared from the command line)
if (!exists("plot.format")) {
  plot.format <- c("pdf", "png");
}
feature.general.info <- list()
xseq.pred.mut.scores <- list()
out.folders <- list()
mut.types <- "both"
project.name <- "BASIS"


##########################
# pri-miR  data to test ##
##########################
# results.dir <- file.path("/storage/mathelierarea/processed/jamondra/Projects/dysmir/BASIS/TEST")
# feature.type <- "premiRNAs"
# mut.tab.file <- "/storage/scratch/TCGA/BASIS/Merged_mutation_table_BASIS.tab"
# expr.tab.file <- "/storage/mathelierarea/processed/jamondra/Projects/dysmir/BASIS/results_436_samples/expression/miRNAs/miRNA_expression_table_BASIS.tab"
# miR.premiR.tab.file <- "/storage/mathelierarea/processed/jamondra/Projects/dysmir/BASIS/results_436_samples/global/tables/miRNA_info/hsa_mir_to_premir.tsv"
# expr.tab.file <- "/storage/scratch/TCGA/BASIS/BASIS_cpm_log2.txt"


## Create output folders
# results.dir <- file.path(results.dir, prefix.name)
out.folders[["tables"]] <- file.path(results.dir, "tables")
out.folders[["plots"]] <- file.path(results.dir, "plots")
trash <- sapply(out.folders, dir.create, recursive = TRUE, showWarnings = FALSE)


#########################
## Load mutation table ##
#########################
message("; Reading input tables")
message("; Reading mutation table: ", mut.tab.file)
mut.tab <- fread(mut.tab.file, sep = "\t", header = TRUE)
mut.tab$sample <- as.character(mut.tab$sample)


###########################
## Load expression table ##
###########################
message("; Reading expression table: ", expr.tab.file)
expr.tab <- read.csv(expr.tab.file, sep = "\t", header = TRUE)

## The sample IDs should be the rownames
colnames(expr.tab) <- as.vector(sapply(names(expr.tab), function(x){ gsub("\\.", "-", x)}))
rownames(expr.tab) <- as.character(rownames(expr.tab))

if(feature.type == "PCG"){
  rownames(expr.tab) <- as.vector(expr.tab$X)
  expr.tab <- expr.tab[,-1]
}



####################################################
## Keep samples with mutation and expression data ##
####################################################
message("; Selecting samples with both mutation and expression data")
mut.expr.samples <- intersect(rownames(expr.tab), mut.tab$sample)
expr.tab <- expr.tab[mut.expr.samples,]
mut.tab <- mut.tab[mut.tab$sample %in% mut.expr.samples,]

dim(expr.tab)
dim(mut.tab)


#########################################
## Load miR - premiR association table ##
#########################################
if(feature.type == "premiRNAs"){
  message("; Reading miR-premiR association table: ", miR.premiR.tab.file)
  miR.premiR.tab <- fread(miR.premiR.tab.file, sep = "\t", header = FALSE)
  colnames(miR.premiR.tab) <- c("miR", "premiR")
  
  miR.premiR.tab$miR <- tolower(miR.premiR.tab$miR)
  miR.premiR.tab$premiR <- tolower(miR.premiR.tab$premiR)
  
  
  ## Add a new column with the number of times each miR is found on different pre-miR
  miR.premiR.tab$nb_miR_dif_loci <- sapply(miR.premiR.tab$miR, function(m){
    length(which(miR.premiR.tab$miR == m))
  })
  
  ## Add a new column with the premiRs associated to each miR
  miR.premiR.tab$premiRs_associated_to_miR <- sapply(miR.premiR.tab$miR, function(m){
    selected.premiR <- as.vector(subset(miR.premiR.tab, miR == m)[,2])
    selected.premiR.print <- paste(selected.premiR, collapse = ",")
  })
  
  ## Add a new column with the number of miRs within the selected pre-miR
  miR.premiR.tab$nb_miR_in_premiR <- sapply(miR.premiR.tab$premiR, function(p){
    length(which(miR.premiR.tab$premiR == p))
  })
  
  ## Add a new column with the names of the miRs within the selected pre-miR
  miR.premiR.tab$miR_in_premiR <- sapply(miR.premiR.tab$premiR, function(p){
    selected.miR <- as.vector(subset(miR.premiR.tab, premiR == p)[,1])
    selected.miR.print <- paste(selected.miR, collapse = ",")
  })
  
  ## Create a list with the miRNA (content) - pre-miRNA (names) association
  miR.to.premiR <- miR.premiR.tab$miR
  names(miR.to.premiR) <- miR.premiR.tab$premiR
  
  ## Create a list with the pre-miRNA (content) - miRNA (names) association
  premiR.to.miR <- miR.premiR.tab$premiR
  names(premiR.to.miR) <- miR.premiR.tab$miR
  

  ## Select miRNA in the mutation table
  mut.tab <- mut.tab[grepl(mut.tab$hgnc_symbol, pattern="hsa-")]
  mut.tab.filt <- merge(mut.tab, miR.premiR.tab, by.x = "hgnc_symbol", by.y = "premiR")
  mut.tab.filt$hgnc_symbol <- mut.tab.filt$miR 
  
} else {
  mut.tab.filt <- mut.tab
}



##########################################
## Normalizing data (required) for xseq ##
##########################################
message("; xseq - Estimating gene expression")
xseq.weights.pdf <- file.path(out.folders[["plots"]], paste0("Weights_distrib_xseq_cis_", feature.type, ".pdf"))
pdf(xseq.weights.pdf)
## Compute whether a gene is expressed in the studied tumour type. 
## If the expression data are from microarray, there is not need to compute weights. 
weight <- EstimateExpression(expr.tab,
                             show.plot = TRUE)
dev.off()


## Impute missing values
expr <- ImputeKnn(expr.tab)


## Quantile-Normalization
## Check this link: https://en.wikipedia.org/wiki/Quantile_normalization
## NOTE: not required because the miR expression table is already normalized
if(feature.type == "premiRNAs"){
  expr.quantile <- expr
} else {
  expr.quantile <- QuantileNorm(expr)
}


######################
## Initialize model ##
######################
message(";xseq - Get expression distribution")
## Show distribution without copy number alterations
expr.dis.quantile <- GetExpressionDistribution(expr=expr.quantile)

# id <- weight[mut.tab[, "hgnc_symbol"]] >= 0.80
# id <- id[!is.na(id)]
# id <- names(id)
weight.threshold <- 0.7
ids <- names(weight[which(weight >= weight.threshold)])

## Select those miR with high expression
mut.filt <- mut.tab.filt[which(mut.tab.filt$hgnc_symbol %in% ids),]


################################################################
## Compare the weight ranks with the median expression values ##
################################################################
message("; xseq - Weights (predicted expression distribution) vs Median expression distribution (observed)")
median.expr.tab <- melt(expr.tab) %>%
  group_by(variable) %>%
  summarize(Median_expr = median(value))
median.expr.tab$Weight <- weight

## Weight vs Expression
weight.vs.expr <- ggplot(data = median.expr.tab, aes(y = Weight, x = Median_expr)) +
  geom_point() +
  theme_classic() +
  labs(title = "Weight vs Median expression") +
  geom_hline(yintercept = weight.threshold, color = "red")

## Weight histogram
weight.dist <- ggplot(data = median.expr.tab, aes(x = Weight)) +
  geom_histogram(bins = 20) +
  theme_classic() +
  labs(title = "Distribution of weights (xseq expression estimation)") +
  geom_vline(xintercept = weight.threshold, color = "red")

## Median expression        
median.expr.distr <- ggplot(data = median.expr.tab, aes(x = Median_expr)) +
  geom_histogram(bins = round(max(median.expr.tab$Median_expr))+1) +
  theme_classic() +
  labs(title = "Distribution of Median Expression")

## Arrange the three plots in a single PDF file
ggarrange(weight.vs.expr, weight.dist, median.expr.distr,
          heights = c(2.95, 2.95, 2.95),
          widths = c(1.95, 1.95, 1.95),
          ncol = 3,
          nrow = 1,
          align = "h",
          common.legend = FALSE)
weights.vs.median.expr.pdf <- file.path(out.folders[["plots"]], paste0("Weights_vs_expression_distr_", feature.type,".pdf"))
ggsave(weights.vs.median.expr.pdf, device = "pdf", width = 10, height = 7, limitsize = FALSE)


########################################################################
## Calculate the posterior probabilities according the mutation types ##
########################################################################
message("; xseq - Calculating posterior probabilities")
em.iterations <- 75

## Mutation effects: all (premiRNA_+_TFBS), TFBS, premirna
if(feature.type == "premiRNAs"){
  mutation.effect <- c("premiRNA_+_TFBS", unique(mut.filt$Mut_class))
  
} else if(feature.type == "PCG"){
  mutation.effect <- c("LoF_+_TFBS", "LoF", "TFBS")
}

xseq.pred.all <- list()
xseq.post.prob <- list()

# plan(multiprocess, workers = length(mutation.effect))
xseq.pred.list <- sapply(mutation.effect, function(me){
  
  mut.filt.by.effect <- NULL
  
  ## Filter the mutation by effect
  if( me == "premiRNA_+_TFBS"){
    
    ## Update the mutation table
    mut.filt.by.effect <- mut.filt
    
  } else if( me == "premiRNA"){
    
    ## Update the mutation table
    mut.filt.by.effect <- mut.filt[mut.filt$Mut_class %in% "premiRNA", ]
    
  } else if( me == "TFBS"){
    
    ## Update the mutation table
    mut.filt.by.effect <- mut.filt[mut.filt$Mut_class %in% "TFBS", ]
    
  } else if( me == "LoF"){
    
    ## Update the mutation table
    mut.filt.by.effect <- mut.filt[mut.filt$Mut_class %in% "LoF", ]
    
  } else if( me == "LoF_+_TFBS"){
    
    ## Update the mutation table
    mut.filt.by.effect <- mut.filt
    
  }
  message("; Running xseq cis premiRNAs - Mutations: ", me, " - Nb: ", nrow(mut.filt.by.effect))
  
  
  if(nrow(mut.filt.by.effect) > 1){
    
    ########################################################################
    ## Calculate the posterior probabilities according the mutation types ##
    ########################################################################
    # mut.types <- c("both", "loss", "gain")
    mut.types <- c("both")
    init <- list()
    model.cis <- list()
    model.cis.em <- list()
    xseq.pred <- list()
    # plan(multiprocess, workers = length(mut.types))
    lapply(mut.types, function(m){
      
      message(";xseq - Mutation type: ", m)
      
      ## Set xseq priors
      message(";xseq - Set priors")
      init[[me]][[m]] <- SetXseqPrior(expr.dis = expr.dis.quantile,
                                      mut <- mut.filt.by.effect,
                                      mut.type <- m,
                                      cis <- TRUE)
      
      constraint <- list(equal.fg=FALSE)
      
      ## Initiate model
      message(";xseq - Initiallizing model")
      model.cis[[me]][[m]] <- InitXseqModel(mut = mut.filt.by.effect,
                                            expr = expr.quantile,
                                            expr.dis = expr.dis.quantile,
                                            cpd = init[[me]][[m]]$cpd,
                                            cis = TRUE,
                                            prior = init[[me]][[m]]$prior,
                                            debug = FALSE)
      
      ## EM step
      message(";xseq - Learning parameters (EM)")
      model.cis.em[[me]][[m]] <- LearnXseqParameter(model = model.cis[[me]][[m]],
                                                    constraint = constraint,
                                                    iter.max = em.iterations,
                                                    threshold = 1e-6,
                                                    cis = TRUE,
                                                    debug = FALSE, 
                                                    show.plot = FALSE)
      
      
      ## Print table with posterior probabilities
      message(";xseq - Exporting table with posterior probabilities: ", me)
      
      ## Export the poterior probabilities table
      post.prob <- model.cis.em[[me]][[m]]$posterior
      xseq.post.prob[[me]] <<- post.prob
      
      xseq.pred <- NULL
      xseq.pred <- ConvertXseqOutput(model.cis.em[[me]][[m]]$posterior)
      colnames(xseq.pred) <- c("sample", "gene", "prob_mut", "prob_gene")
      xseq.pred$project_name <- project.name
      xseq.pred$mut_type <- m
      xseq.pred$mut_effect <- me
      
      ## Export the xseq table
      # xseq.table.file <- file.path(out.folders[["tables"]], paste0("xseq_table_mut_type_", m, ".tab"))
      # fwrite(xseq.pred, file = xseq.table.file, sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)
      
      xseq.pred
    })
  }
})
## Concatenate all the lists
xseq.pred.all.concat <- do.call(rbind, xseq.pred.list)

xseq.table.postprob.file <- file.path(out.folders[["tables"]], paste0("xseq_post_prob_table_all_mut_types_", feature.type,"_", project.name, ".Rdata"))
save(xseq.post.prob, file = xseq.table.postprob.file)


xseq.results.file <- file.path(out.folders[["tables"]], paste0("xseq_cis_", feature.type, "_all_mutation_types_all_effects_", project.name, ".tab"))
fwrite(xseq.pred.all.concat, file = xseq.results.file, sep = "\t", quote = FALSE, row.names = FALSE, col.names = TRUE)

## Save the data as Rdata
xseq.results.rdata <- file.path(out.folders[["tables"]], paste0("xseq_cis_", feature.type, "_all_mutation_types_all_effects_", project.name, ".Rdata"))
save(xseq.pred.all.concat, file = xseq.results.rdata)

# xseq.pred.all.concat %>% 
#   filter(prob_gene >= 0.5)