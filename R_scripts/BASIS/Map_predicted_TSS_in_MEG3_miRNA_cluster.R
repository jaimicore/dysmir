#####################
## Load R packages ##
#####################
required.libraries <- c("data.table")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    library(lib, character.only=TRUE)
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message(";Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("results.dir")) {
  stop("Missing mandatory argument (Results folder): results.dir ")
} else if (!exists("tss.files.folder")) {
  stop("Missing mandatory argument (TSS folder): tss.files.folder ")
} else if (!exists("region.bed.file")) {
  stop("Missing mandatory argument (Region to intersect BED): region.bed.file ")
}


##########################
# pri-miR  data to test ##
##########################
# tss.files.folder <- "/home/jamondra/Downloads/Archive"
# results.dir <- file.path("results/MEG3_miR_cluster")
# region.bed.file <- ""

###########################
## Create results folder ##
###########################
message(";Creating result folders")
folders.to.create <- list()
folders.to.create[["TSS_BED"]] <- file.path(results.dir, "putative_TSS_BED")
thrash <- sapply(folders.to.create, dir.create, recursive = TRUE, showWarnings = FALSE)


##########################################
## Create a list with all the TSS files ##
##########################################
tss.files.subfolder <- file.path(tss.files.folder, list.files(tss.files.folder))
tss.files.list <- sapply(tss.files.subfolder, list.files)

tss.files <- file.path(names(tss.files.list[1]), tss.files.list[[1]])
tss.files <- c(tss.files,
            file.path(names(tss.files.list[2]), tss.files.list[[2]]))

names(tss.files) <- as.vector(unlist(tss.files.list))


#################################################
## Generate a BED file containing all the TSSs ##
#################################################

## Iterate over the TSS files
all.tss.bed <- data.frame()
for(f in names(tss.files)){
  
  message("; Parsing TSS from: ", f)
  
  ## Depending of the file, the coordinates may be in different formats
  ## Match the file name in a category an process the file
  
  
  ####################################
  ## Chien_2011_hg19_TSS.txt format ##
  ####################################
  if(f == "Chien_2011_hg19_TSS.txt" 
     | f == "Georgakilas_2014_hg19_TSS.txt" 
     | f == "Marsico_2013_hg19_TSS.txt"
     | f == "Marson_2008_hg19_TSS.txt"){
    
    ## Read the file
    tss.file <- read.csv(tss.files[f],
                         sep = "\t",
                         header = FALSE, 
                         comment.char = "#")
    
    ## Create missing 
    tss.bed <- tss.file[,1:3]
    tss.bed$end <- as.numeric(tss.file$V3) + 1
    tss.bed$score <- "."
    tss.bed$strand <- "+"
    
    ## Order the columns + rename
    tss.bed <- tss.bed[, c(2,3,4,5,1,6)]
    colnames(tss.bed) <- c("chr", "start", "end", "score", "name", "strand")
  }
  

  ##############################
  ## TSS in cell lines format ##
  ##############################
  else if( grepl(pattern = "\\w+\\.txt", x = f) ){
    
    ## Read the file
    tss.file <- read.table(tss.files[f],
                      sep = "\t",
                      header = TRUE)
    
    ## Select relevant columns
    tss.bed <- tss.file[, c("chrom", "predicted_TSS", "MIR_ID", "strand")]
    
    ## Create missing columns
    tss.bed$end <- tss.bed$predicted_TSS + 1
    tss.bed$score <- "."
    
    ## Order the columns + rename
    tss.bed <- tss.bed[, c(1,2,5,6,3,4)]
    colnames(tss.bed) <- c("chr", "start", "end", "score", "name", "strand")
    
    
    
  }
  
  ## Concatenate the parsed BED file
  all.tss.bed <- rbind(all.tss.bed, tss.bed)

}

## Remove duplicated TSS
all.tss.bed <- unique.data.frame(all.tss.bed)

## Export BED file 
all.tss.bed.file <- file.path(folders.to.create[["TSS_BED"]], "All_putative_miRNA_TSSs.bed")
message(" ;Exporting all putative TSS: ", all.tss.bed.file)
write.table(all.tss.bed,
             file = all.tss.bed.file,
             sep = "\t",
             quote = FALSE,
             col.names = FALSE,
             row.names = FALSE)


############################################
## Find TSS within the region of interest ##
############################################

all.tss.in.region.bed.file <- file.path(folders.to.create[["TSS_BED"]], "All_putative_miRNA_TSSs_in_MEG3_region.bed")

## Find the TSS within the selected region
bedtools.command <- paste0("bedtools intersect -a ", all.tss.bed.file, " -b ", region.bed.file, " -wa | cut -f1,2,3 | sort | uniq > ", all.tss.in.region.bed.file)
message("Command: ", bedtools.command)
system(bedtools.command)
