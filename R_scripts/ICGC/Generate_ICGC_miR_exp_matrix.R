#####################
## Load R packages ##
#####################
required.libraries <- c("dplyr",
                        "edgeR",
                        "ggplot2",
                        "ggridges",
                        "reshape2",
                        "viridis")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    library(lib, character.only=TRUE)
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message(";Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("mapped.miRs.bed.file")) {
  stop("Missing mandatory argument (Expression values in BED format): mapped.miRs.bed.file ")
  
} else if (!exists("expr.miR.tab.clean.file")) {
  stop("Missing mandatory argument (Normalized and clean  miRNA expression table file path): expr.miR.tab.clean.file ")
  
} else if (!exists("speciment.list.file")) {
  stop("Missing mandatory argument (Tumor sample speciment list file path): speciment.list.file ")
  
} else if (!exists("mir.premir.tab.file")) {
  stop("Missing mandatory argument (miRNA <-> pre-miRNA association table): mir.premir.tab.file ")
  
}


## Debug
# mapped.miRs.bed.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/dysmiR_revised_version_input_20-08-2018/mapped_premiR_ICGC_BRCA-US.bed"
# speciment.list.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/dysmiR_revised_version_input_20-08-2018/Specimen_Donor_information_Tumour_BRCA-US_WGS.tab"
# mir.premir.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/dysmiR_revised_version_input_20-08-2018/hsa_mir_to_premir.tsv"
# plots.folder <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/dysmiR_revised_version_input_20-08-2018/miRNA_expression_distribution/Test"


#########################
## Load speciment list ##
#########################
speciment.list <- read.csv(speciment.list.file, header = TRUE, sep = "\t")
speciment.list <- data.frame(speciment.list[,1])
colnames(speciment.list) <- c("Sample")


#######################################
## Load miR-premiR association table ##
#######################################
mir.premir.tab <- read.csv(mir.premir.tab.file, header = FALSE, sep = "\t")
colnames(mir.premir.tab) <- c("miR", "premiR")
mir.premir.tab$miR <- tolower(mir.premir.tab$miR)
mir.premir.tab$premiR <- tolower(mir.premir.tab$premiR)
# mir.premir.tab$mature_identifier <- paste(mir.premir.tab$miR, mir.premir.tab$premiR, sep = "::")
# View(mir.premir.tab)


#################################
## Load input expression table ##
#################################
miR.expr.bed.file <- mapped.miRs.bed.file
miR.expr.bed <- read.csv(miR.expr.bed.file, header = FALSE, sep = "\t")
miR.expr.bed <- miR.expr.bed[c(7,8,9)]
colnames(miR.expr.bed) <- c("Sample", "Value", "premiR")
miR.expr.bed$Sample <- as.vector(miR.expr.bed$Sample)
miR.expr.bed$Value <- as.vector(miR.expr.bed$Value)
miR.expr.bed$premiR <- as.vector(miR.expr.bed$premiR)

## Select those specimens from tumor samples
# dim(miR.expr.bed)
miR.expr.bed <- miR.expr.bed[miR.expr.bed$Sample %in% speciment.list$Sample, ]
# dim(miR.expr.bed)


## more results_ICGC_breast_938_samples/parsed_tables/other/mapped_premiR_ICGC.bed | cut -f9 | sort | uniq -c | grep -v 1051 | less
## Remove the following premiRs - To count values in the table, there is no way to know wich value to consider.
# miRs.to.rm <- c("hsa-mir-214",
#                 "hsa-mir-24-1",
#                 "hsa-mir-3065",
#                 "hsa-mir-3074",
#                 "hsa-mir-3116-1",
#                 "hsa-mir-3116-2",
#                 "hsa-mir-3119-1",
#                 "hsa-mir-3119-2",
#                 "hsa-mir-3120",
#                 "hsa-mir-3130-1",
#                 "hsa-mir-3130-2",
#                 "hsa-mir-3158-1",
#                 "hsa-mir-3158-2",
#                 "hsa-mir-3160-1",
#                 "hsa-mir-3160-2",
#                 "hsa-mir-3184",
#                 "hsa-mir-3190",
#                 "hsa-mir-3191",
#                 "hsa-mir-3199-1",
#                 "hsa-mir-3199-2",
#                 "hsa-mir-3202-1",
#                 "hsa-mir-3202-2",
#                 "hsa-mir-338",
#                 "hsa-mir-423")
# samples.to.rm <- c("SP3016")
# head(miR.expr.bed)

## Remove samples and premiRs with several entries.
## Ideally, each sample should appear X times, where x is the number os premiRs
## and each premiR should appears P times, where P is the number of samples.

nb.premiRs <- as.numeric(names(table(table(miR.expr.bed$Sample))[1]))
selected.samples <- names(which(table(miR.expr.bed$Sample) == nb.premiRs))

nb.samples <- as.numeric(names(table(table(miR.expr.bed$premiR))[1]))
selected.premiRs <- names(which(table(miR.expr.bed$premiR) == nb.samples))


## Remove those repeated miRs
# miR.expr.bed <- miR.expr.bed[!miR.expr.bed$premiR %in% miRs.to.rm, ]
# miR.expr.bed <- miR.expr.bed[!miR.expr.bed$Sample %in% samples.to.rm, ]
# miR.expr.bed <- unique.data.frame(miR.expr.bed)


## Keep the selected samples and premiRs
miR.expr.bed <- miR.expr.bed[miR.expr.bed$premiR %in% selected.premiRs, ]
miR.expr.bed <- miR.expr.bed[miR.expr.bed$Sample %in% selected.samples, ]

## Cast the file into a matrix
miR.expr.bed.casted <- acast(miR.expr.bed, Sample ~ premiR, value.var = "Value")


#######################################
## Normalize + transform counts: cpm ##
#######################################
# miR.expr.bed.casted2 <- miR.expr.bed.casted
# miR.expr.bed.casted <- miR.expr.bed.casted2

samples.ids <- rownames(miR.expr.bed.casted)
expr.miR.tab <- miR.expr.bed.casted
rownames(expr.miR.tab) <- samples.ids

## Transpose the table - This is required for the xseq input
## Columns (after transposition): gene ID
## Rows (after transposition): sample ID

## The matrix must be transposed to apply the cpm
expr.miR.tab <- t(expr.miR.tab)

## Start

# ## Raw counts All: Save the table to plot the data
# expr.miR.tab.melt.raw <- melt(expr.miR.tab)
# expr.miR.tab.melt.raw <- expr.miR.tab.melt.raw[,c(1,3)]
# colnames(expr.miR.tab.melt.raw) <- c("premiR", "Counts")
# expr.miR.tab.melt.raw$Counts <- log2(expr.miR.tab.melt.raw$Counts + 1)
# 
# 
# ## First filter: Keep only the genes with at least X UMI counts across all samples 
# ## (e.g. the total number the gene would have, if it was expressed with a value of 3 in 10% of the samples).
# # min.reads <- 3 * 0.05 * ncol(expr.miR.tab) ## 06-05-2020
# min.reads <- 3 * 0.1 * ncol(expr.miR.tab) ## Values from SCENIC
# 
# genesLeft.minReads <- rownames(expr.miR.tab)[rowSums(expr.miR.tab) >= min.reads ]
# length(genesLeft.minReads)
# 
# ## Apply the first filter
# expr.miR.tab <- expr.miR.tab[rownames(expr.miR.tab) %in% genesLeft.minReads , ]
# dim(expr.miR.tab)
# 
# ## Raw counts First Filter: Save the table to plot the data
# expr.miR.tab.melt.filter1 <- melt(expr.miR.tab)
# expr.miR.tab.melt.filter1 <- expr.miR.tab.melt.filter1[,c(1,3)]
# colnames(expr.miR.tab.melt.filter1) <- c("premiR", "Counts")
# expr.miR.tab.melt.filter1$Counts <- log2(expr.miR.tab.melt.filter1$Counts + 1)
# 
# 
# ## Second filter: Keep the genes that are detected in at least 10% of the samples.
# ## This filtering is meant to remove genes whose reads come from a few ‘noisy’ samples.
# # min.expr <- 64 # log2(64) = 6 ## 06-05-2020
# min.expr <- 1 ## Values from SCENIC
# min.samples <- round(ncol(expr.miR.tab) * 0.1)
# 
# 
# ## Each row correspond to a gene in the expression table.
# ## 1. Count how many samples have the minimum expression.
# ## 2. Evaluate if the number got from step 1 is at least the 10% of the population.
# ## If so return a TRUE value
# selected.genes.expr <- apply(expr.miR.tab, 1, function(r){
#   
#   if( sum(r >= min.expr) >= min.samples ){
#     TRUE
#     
#   } else {
#     FALSE
#   }
# })
# expr.miR.tab <- expr.miR.tab[selected.genes.expr, ]
# dim(expr.miR.tab)
# 
# 
# ## Raw counts First Filter: Save the table to plot the data
# expr.miR.tab.melt.filter2 <- melt(expr.miR.tab)
# expr.miR.tab.melt.filter2 <- expr.miR.tab.melt.filter2[,c(1,3)]
# colnames(expr.miR.tab.melt.filter2) <- c("premiR", "Counts")
# expr.miR.tab.melt.filter2$Counts <- log2(expr.miR.tab.melt.filter2$Counts + 1)

## END

expr.miR.tab <- DGEList(expr.miR.tab)
expr.miR.tab <- calcNormFactors(expr.miR.tab,
                                method = "none")
# expr.miR.tab <- calcNormFactors(expr.miR.tab,
#                                 method = "TMM",
#                                 doWeighting = TRUE)

## Obtain the CPM matrix
## The filtering must be donde after filtering
expr.miR.tab <- cpm(expr.miR.tab)

selected.genes <- apply(expr.miR.tab, 1, function(x){
  
  nb.samples <- round(ncol(expr.miR.tab)/2)
  
  sum(x > 0) >= nb.samples
  
})
selected.genes <- names(which(selected.genes == T))

expr.miR.tab <- expr.miR.tab[rownames(expr.miR.tab) %in% selected.genes,]


## log2 transformation
expr.miR.tab <- log2(expr.miR.tab + 1)

## Raw counts First Filter: Save the table to plot the data
expr.miR.tab.melt.cpm <- melt(expr.miR.tab)
expr.miR.tab.melt.cpm <- expr.miR.tab.melt.cpm[,c(1,3)]
colnames(expr.miR.tab.melt.cpm) <- c("premiR", "Counts")

expr.miR.tab <- t(expr.miR.tab)

## Re-built the matrix with new columns (mature miRNAs)
expr.miR.tab.mapped.miRs <- expr.miR.tab


######################################################################################
## Draw the miRNA expression distribution thrhough the filtering/normalization steps ##
######################################################################################
# expr.tables <- list(expr.miR.tab.melt.raw, expr.miR.tab.melt.filter1, expr.miR.tab.melt.filter2, expr.miR.tab.melt.cpm)
# table.names <- c("Raw_counts", "Raw_counts_Filter1", "Raw_counts_Filter2", "CPM")
# counter <- 1
# sapply(expr.tables, function(tab){
#   
#   ## Calculate the meadin and SD of each premiR
#   tab <- tab %>%
#     group_by(premiR) %>%
#     mutate(median_expr = round(median(Counts), digits = 2),
#            sd_expr = round(var(Counts), digits = 2))
#   
#   ## Order the table by median expression
#   tab <- tab[order(tab$median_expr, tab$premiR), ]
#   
#   ## Plot the distributions
#   distr.prefix <- table.names[counter]
#   distr.plot.file <- file.path(plots.folder, paste0(distr.prefix, "_expression_distribution_miRNAs.pdf"))
#   distr.plot <- ggplot(tab, aes(x = Counts, y = premiR, fill = median_expr)) + 
#                   geom_density_ridges2(jittered_points = FALSE, alpha = 0.6, scale = 0.9) +
#                   scale_fill_viridis(name = "Median Expression", direction = -1, option = "B") +
#                   theme_classic() + 
#                   scale_y_discrete(limits = as.vector(unlist(unique(tab$premiR)))) ## Use this line to sort the y-axis
#   ggsave(plot = distr.plot, filename = distr.plot.file, height = round(length(unique(tab$premiR))/2), width = 30, limitsize = FALSE)
#   
#   counter <<- counter + 1
#   
# })


#########################
## Export the matrices ##
#########################
message("; Exporting expression matrix: ", expr.miR.tab.clean.file)
write.table(expr.miR.tab.mapped.miRs, file = expr.miR.tab.clean.file, quote = FALSE, col.names = TRUE, row.names = TRUE, sep = "\t")