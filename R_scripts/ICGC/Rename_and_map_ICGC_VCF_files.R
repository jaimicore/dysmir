##################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message("; Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


#########################
## Mandatory arguments ##
#########################
if (!exists("cancer.projects")) {
  stop("Missing mandatory argument (Cancer project names): cancer.projects ")
  
}


########################
## Required libraries ##
########################
library("jsonlite")
library("dplyr")
library("data.table")
library("future.apply")


##########################
## Initialize variables ##
##########################

## Set working directory
main.dir <- "/storage/scratch/TCGA/ICGC/Variant_calling_download/"
# setwd("/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/other/Space_estimation")

setwd(main.dir)

# cancer.projects <- c( "BLCA-US", 
#                       "BRCA-US",
#                       "CESC-US",
#                       "COAD-US",
#                       "HNSC-US",
#                       "KIRC-US",
#                       "KIRP-US",
#                       "LGG-US",
#                       "LIHC-US",
#                       "LUAD-US",
#                       "LUSC-US",
#                       "OV-US",
#                       "PRAD-US",
#                       "READ-US",
#                       "SKCM-US",
#                       "STAD-US",
#                       "THCA-US",
#                       "UCEC-US")

## Iterate over tha cancer projects
for(p in cancer.projects){
  
  ####################
  ## Define folders ##
  ####################
  binary.files <- file.path(p, "binary") ## Raw data
  vcf.files <- file.path(p, "VCF") ## After mapping IDs; Renamed with the IDs
  vcf.post.files <- file.path(vcf.files, "post_process") ## Files for process the VCF
  
  
  ##########################################
  ## Evaluate if the pipeline will be ran ##
  ##########################################
  ## Execute the pipeline only if the final file do not exist
  ## Set the name for the final file (concatenated VCF files per project)
  vcf.cat.all.file <- file.path(vcf.post.files, paste0(p, "_mutations_all_samples_concat.VCF"))
  if( file.exists(vcf.cat.all.file) ){
    next()
  }
  message("; Procesing files for ", p)
  
  ####################################
  ## Read JSON table to get file ID ##
  ####################################
  
  ## Read the JSON file (with the somatic mutation information)
  json.file <- file.path("JSON", "Sanger_pipeline", paste0(p, "_mutation_files.json"))
  json.ICGC.tab <- fromJSON(json.file, simplifyDataFrame = TRUE)
  # View(json.ICGC.tab)
  
  ## Extract the data.frames stored in the columns of the table after read from JSON
  ## File information
  json.ICGC.data.tab <- json.ICGC.tab
  json.ICGC.tab.file.col <- do.call(rbind, json.ICGC.data.tab$fileCopies)
  json.ICGC.tab.file.col <- data.frame(fileName = unique(json.ICGC.tab.file.col$fileName)) ## Every entry appears twice, one ofr TCGA and for pdc. 
  
  ## Donor information
  json.ICGC.tab.donor.col <- do.call(rbind, json.ICGC.data.tab$donors)
  
  ## Bind the columns
  json.ICGC.data.tab <- cbind(json.ICGC.data.tab[,1:3],json.ICGC.tab.file.col, json.ICGC.tab.donor.col)
  
  ## Select relevant fields
  json.ICGC.data.tab <- json.ICGC.data.tab[, c("id", "fileName", "donorId", "specimenId", "sampleId")]
  
  ## Get the file Id from the fileName column
  json.ICGC.data.tab$fileID <- gsub(x = json.ICGC.data.tab$fileName, pattern = "\\.svcp_.+\\.somatic\\.\\w+\\.vcf.gz", replacement = "")
  
  ## Create a new column with the fileID withouth the last 4 characters
  ## The file ID found in the binary file match the first 32 characters of the ID (36 characters)
  ## This step is exectured to further merge the matrices
  sep.char.IDs <- sapply(json.ICGC.data.tab$fileID, strsplit, split = "")
  json.ICGC.data.tab$fileID_parsed <- sapply(sep.char.IDs, function(s){ string.size <- length(s); paste(s[1:(string.size-4)], collapse = "") }) ## Remove the last 4 characters
  # View(json.ICGC.data.tab)
  

  ################################################################
  ## Map the sample/specimen IDs from the VCF to the ICGC table ##
  ################################################################
  
  ## 1) Extract the ID from the VCF file
  ## 2) Extract the algorithm name: caveman|pindel
  IDs.software.tab <- data.frame()
  
  ## Get the file names
  vcf.bin.files <- list.files(binary.files)
  vcf.bin.files <- vcf.bin.files[!grepl(vcf.bin.files, pattern = ".sh")] ## Remove the manifest file from the list of files
  
  ## Send the output to silence, otherwise it will print it
  ## Set parallelization
  plan(multiprocess)
  
  IDs.software.tab <- future_sapply(vcf.bin.files, function(b){
    
    bin.file <- file.path(binary.files, b)
    
    ################################################
    ## 1) Extract the software name
    
    ## 1. Search the pattern '##INFO' in any file and the word 'call'.
    ## 2. Extract the first line (in case there are more than one).
    ## 3. Match the software names: pindel or caveman
    
    get.software.name.command <- paste0("bcftools view ", bin.file, " | head -n 150 | grep '##INFO' | grep call | head -n 1 ")
    software.name <- system(get.software.name.command, intern = TRUE)
    
    ################################################
    ## 2) Extract the ID using regular expression
    ## Assign the software name
    ## Based on the software name get the line with the sample identifier
    if(grepl(pattern = "pindel", ignore.case = TRUE, x = software.name)){
      
      software.name <- "pindel" ## Software name
      get.sample.command <- paste0("bcftools view ", bin.file, " | head -n 150 | grep '##SAMPLE' | grep Mutant") ## Patter searched in the pindel file
      
    } else if(grepl(pattern = "caveman", ignore.case = TRUE, x = software.name)){
      
      software.name <- "caveman" ## Software name
      get.sample.command <- paste0("bcftools view ", bin.file, " | head -n 150 | grep '##SAMPLE' | grep Tumour") ## Patter searched in the caveman file
    }
    
    ## Run the command
    bin.file.ID <- system(get.sample.command, intern = TRUE)

    ## Match the ID using regular expression
    ID.regexp <- gregexpr("\\w{1,8}-\\w{1,4}-\\w{1,4}-\\w{1,4}-\\w{1,8}", bin.file.ID, perl=TRUE) ## Define the regular expression
    bin.file.ID <- unlist(regmatches(bin.file.ID , ID.regexp)) ## Extract the match
    
    ## Save the IDs and the software in a table
    message("ID: ", bin.file.ID, " - Software: ", software.name)
    ids.df <- c(ID_in_vcf = bin.file.ID,
               ID_bin_name = b,
               Program = software.name)
    ids.df
    
  })
  IDs.software.tab <- as.data.table(t(data.frame(IDs.software.tab)))
  
  
  ## Merge the tables: JSON + bin files
  IDs.to.files.mapped.tab <- merge(x = json.ICGC.data.tab[c("donorId", "specimenId", "sampleId", "fileID_parsed")],
                                   y= IDs.software.tab,
                                   by.x = "fileID_parsed",
                                   by.y = "ID_in_vcf", no.dups = TRUE)
  IDs.to.files.mapped.tab <- unique.data.frame(IDs.to.files.mapped.tab)


  ## Export Specimen <-> File ID table
  dir.create(vcf.post.files, recursive = TRUE, showWarnings = FALSE)
  IDs.to.files.mapped.tab.file <- file.path(vcf.post.files, paste0(p, "_map_specimen_IDs_with_files.tab"))
  fwrite(IDs.to.files.mapped.tab, file = IDs.to.files.mapped.tab.file, sep = "\t", row.names = FALSE, col.names = TRUE)

  
  ##########################################
  ## Create the VCF from the binary files ##
  ##########################################
  
  ## Set parallelization
  plan(multiprocess)
  
  ## Send the output to silence, otherwise it will print it
  silence <- future_sapply(vcf.bin.files, function(b){ ## b states for 'binary file'
    
    ## Given a binary file name, get (i) the software and (ii) the specimen ID.
    b.line.info <- subset(IDs.to.files.mapped.tab, ID_bin_name == b)
    specimen.id <- b.line.info$specimenId
    software.name <- b.line.info$Program
    new.vcf.file.name.raw <- paste0(specimen.id, "_", software.name, "_raw.vcf")
    
    ## Command to print the bin file as VCF (using bcftools)
    ## The new file name corresponds to the specimenID and the software name
    bin.file <- file.path(binary.files, b)
    new.vcf.file.name.raw <- file.path(vcf.files, new.vcf.file.name.raw)
    create.vcf.cmd <- paste0("bcftools view ", bin.file, " > ", new.vcf.file.name.raw)
    message("; Generating raw VCF file for ", specimen.id, " - ", software.name)
    system(create.vcf.cmd)
  })
  
  
  ##########################################
  ## Add the specimen ID in the VCF files ##
  ##########################################
  ## At the end we want a single VCF file containing all the patients mutations.
  ## To do so, first we need to add the specimen ID to each VCF file, separately.
  vcf.files.raw <- list.files(vcf.files)
  vcf.files.raw <- vcf.files.raw[grepl(vcf.files.raw, pattern = "raw")]

  ## Get the specimen ID and the software names
  vcf.files.name.fields <- data.frame(do.call(rbind, strsplit(vcf.files.raw, split = "_") ))
  vcf.specimen.id <- as.vector(vcf.files.name.fields$X1)
  vcf.software.name <- as.vector(vcf.files.name.fields$X2)
  
  silence <- future_sapply(1:length(vcf.specimen.id), function(i){
    
    ## Set the name for the cleaned VCF files
    vcf.file.clean <- file.path(vcf.files , paste0(vcf.specimen.id[i], "_", vcf.software.name[i], "_clean.vcf"))
    
    ## Command to add the Specimen ID in the VCF file
    ## Clean the file and remove uninformative columns
    clean.vcf.cmd <- paste0(" grep -v '^#' -P ", file.path(vcf.files, vcf.files.raw[i]), " | awk '{ printf \"chr%s\\t%s\\t%s\\t%s\\t%s\\t", vcf.specimen.id[i], "\\n\", $1,$2,$2+1,$4,$5}' >  ", vcf.file.clean)
    message("; Generating clean VCF file for ", vcf.specimen.id[i], " - ", vcf.software.name[i])
    system(clean.vcf.cmd)
  })
  
  
  #############################################
  ## Remove raw files and concat clean files ##
  #############################################
  remove.raw.vcf.cmd <- paste0("rm -rf ", vcf.files,"/*_raw.vcf")
  system(remove.raw.vcf.cmd)
  
  ## Concatenate all the VCF clean files
  concat.raw.vcf.cmd <- paste0("cat ", vcf.files,"/*_clean.vcf > ", vcf.cat.all.file)
  system(concat.raw.vcf.cmd)

}