f5_data=/storage/mathelierarea/processed/anthoma/Projects/BC_miRs/data/20160715_F5_miR_ms
BIN_PYTHON=/storage/mathelierarea/processed/jamondra/Projects/dysmir/python_files
BIN_R=/storage/mathelierarea/processed/jamondra/Projects/dysmir/R_scripts
mirbase=/storage/mathelierarea/processed/anthoma/Projects/BC_miRs/data/20160720_mirbase
python=/usr/bin/python2.7
R=/usr/bin/R
BASIS=/storage/mathelierarea/raw/genetics.rr-research/BASIS_560_cohort
hg19size=/lsc/bedtools2/genomes/human.hg19.genome
DATA=/storage/mathelierarea/processed/anthoma/Projects/BC_miRs/data/
MIR_EXPR_TAB=/storage/mathelierarea/processed/jamondra/miRNA_399_samples_normalized_BASIS_sample_ids_removed_controlspots.txt
MAKEFILE=makefiles/Prepare_miR_data_for_xseq.mk
RESULT_DIR=results
TABLES_PRIMIR_DIR=${RESULT_DIR}/parsed_tables/primiR_data
TABLES_PREMIR_DIR=${RESULT_DIR}/parsed_tables/premir_data
TABLES_OTHER_DIR=${RESULT_DIR}/parsed_tables/other
TABLES_PROMOTER_DIR=${RESULT_DIR}/parsed_tables/promoters_data
TABLES_PROM_PRI_DIR=${RESULT_DIR}/parsed_tables/promoters_primiR_data



################################################################
## List of targets
_usage:
	@echo "usage: make [-OPT='options'] target"
	@echo "implemented targets"
	@perl -ne 'if (/^([a-zA-Z]\S+):/){ print "\t$$1\n";  }' ${MAKEFILE}


################################################################
## Create required folders
create_output_folders:
	mkdir -p ${TABLES_PRIMIR_DIR}
	mkdir -p ${TABLES_PREMIR_DIR}
	mkdir -p ${TABLES_OTHER_DIR}
	mkdir -p ${TABLES_PROMOTER_DIR}
	mkdir -p ${TABLES_PROM_PRI_DIR}

################################################################
## Get the TSSs associated to miRNAs (BED file)
miR_F5_TSS.bed: $(f5_data)/TableS11.tsv
	@echo
	@echo "Creating BED file with TSSs associated to miRs - FANTOM5 data"
	awk '!/^#/{ \
		printf "%s\t%d\t%d\t%s;%s;%s\t.\t%s\n", \
		$$1, $$3-1, $$3, $$4, $$5, $$7, $$2}' $< > ${TABLES_OTHER_DIR}/$@;


################################################################
## Obtain the primiRs from FANTOM5 + miRbase in BED format
##
## The pri-miR is built with the TSS associated to the miR
## to the 3' of the precursor
##
## Note that novel-miRNAs have been ommitted here
primiRs_F5.bed: miR_F5_TSS.bed $(mirbase)/hsav20.gff3
	@echo
	@echo "Creating BED file with pri-miRs"
	$(python) $(BIN_PYTHON)/construct_primiRs.py -t ${TABLES_OTHER_DIR}/$< -m $(mirbase)/hsav20.gff3 -o ${TABLES_PRIMIR_DIR}/$@;


################################################################
## Obtain the premiRs from miRbase in BED format
#
## Note that novel-miRNAs have been ommitted here
premiRs_F5.bed: $(mirbase)/hsav20.gff3
	@echo
	@echo "Creating BED file with pre-miRs"
	$(python) $(BIN_PYTHON)/construct_premiRs.py -m $(mirbase)/hsav20.gff3 -o ${TABLES_PREMIR_DIR}/$@;


################################################################
## Obtain the coordinates (BED file) for the indels 
Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF: $(BASIS)/indels/Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered
	awk ' \
		BEGIN{OFS="\t"; print "##fileformat=VCFv4.0"; \
		print "#CHROM\tPOS\tSample_ID\tID\tREF\tALT\tQUAL\tFILTER\tINFO/tTYPE"} \
		!/^#/{print "chr"$$5,$$6,$$2,$$4,$$7,$$8,$$9,$$10,$$11,"OTHER"}' $< > ${TABLES_OTHER_DIR}/$@;

###############################################################
## Obtain the coordinates (BED file) for the SNVs
Caveman_560_20Nov14_clean.VCF: $(BASIS)/Substitutions/Caveman_560_20Nov14_clean.txt
	awk ' \
		BEGIN{OFS="\t"; print "##fileformat=VCFv4.0"; \
		print "#CHROM\tPOS\tSample_ID\tID\tREF\tALT\tQUAL\tFILTER\tINFO/tTYPE"} \
		!/^#/{print "chr"$$5,$$6,$$2,$$4,$$7,$$8,$$9,$$10,$$11,"OTHER"}' $< > ${TABLES_OTHER_DIR}/$@;

################################################################
## Map and print the mutations overlapping pri-miRNAs
## Promoter region is not included
BASIS_mut_in_primiR.tsv:
	@echo
	@echo "Mapping SNVs on pri-miRNAs"
	${MAKE} _BASIS_subst_in_primiR.tsv
	@echo
	@echo "Mapping indels on pri-miRNAs"
	${MAKE} _BASIS_indels_in_primiR.tsv
	@echo
	@echo "Concatenate mutations on pri-miRNAs"
	cat ${TABLES_OTHER_DIR}/BASIS_subst_in_primiR.tsv ${TABLES_OTHER_DIR}/BASIS_indels_in_primiR.tsv > ${TABLES_PRIMIR_DIR}/BASIS_mut_in_primiR.tsv;

################################################################
## Obtain those mutations (SNVs) lying on the pri-miRNAs
_BASIS_subst_in_primiR.tsv:
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Caveman_560_20Nov14_clean.VCF -b ${TABLES_PRIMIR_DIR}/primiRs_F5.bed > ${TABLES_OTHER_DIR}/BASIS_subst_in_primiR.tsv;

################################################################
## Obtain those mutations (indelss) lying on the pri-miRNAs
_BASIS_indels_in_primiR.tsv:
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b ${TABLES_PRIMIR_DIR}/primiRs_F5.bed > ${TABLES_OTHER_DIR}/BASIS_indels_in_primiR.tsv;


################################################################
## Map and print the mutations overlapping pre-miRNAs
## Promoter region is not included
BASIS_mut_in_premiR.tsv:
	@echo
	@echo "Mapping SNVs on pre-miRNAs"
	${MAKE} _BASIS_subst_in_premiR.tsv
	@echo
	@echo "Mapping indels on pri-miRNAs"
	${MAKE} _BASIS_indels_in_premiR.tsv
	@echo
	@echo "Concatenate mutations on pre-miRNAs"
	cat ${TABLES_OTHER_DIR}/BASIS_subst_in_premiR.tsv ${TABLES_OTHER_DIR}/BASIS_indels_in_premiR.tsv > ${TABLES_PREMIR_DIR}/$@;

################################################################
## Obtain those mutations (SNVs) lying on the pri-miRNAs
_BASIS_subst_in_premiR.tsv:
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Caveman_560_20Nov14_clean.VCF -b ${TABLES_PREMIR_DIR}/premiRs_F5.bed > ${TABLES_OTHER_DIR}/BASIS_subst_in_premiR.tsv;

################################################################
## Obtain those mutations (indelss) lying on the pri-miRNAs
_BASIS_indels_in_premiR.tsv:
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b ${TABLES_PREMIR_DIR}/premiRs_F5.bed > ${TABLES_OTHER_DIR}/BASIS_indels_in_premiR.tsv;


################################################################
## Generate the miR promoters  BED files
## Relative to the TSSs from F5 data
promoters_bed:
	@echo
	@echo "Generating BED files for miR promoters"
	@echo
	${MAKE} _miR_F5_prom-1kb+1kb.bed
	@echo
	${MAKE} _miR_F5_prom-500nt+500nt.bed
	@echo
	${MAKE} _miR_F5_prom-300nt+100nt.bed

_miR_F5_prom-1kb+1kb.bed: ${TABLES_OTHER_DIR}/miR_F5_TSS.bed $(hg19size)
	bedtools slop -b 1000 -g $(hg19size) -i $< | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[3] =~ /hsa-/g){ my @split_prom_info = split(/;/, $$line[3]); my @miRs = split(/,/, $$split_prom_info[2]); foreach my $$miR (@miRs){ print $$line[0]."\t".$$line[1]."\t".$$line[2]."\t".$$miR;} };' > ${TABLES_PROMOTER_DIR}/miR_F5_prom-1kb+1kb.bed;

_miR_F5_prom-500nt+500nt.bed: miR_F5_TSS.bed $(hg19size)
	bedtools slop -b 500 -g $(hg19size) -i ${TABLES_OTHER_DIR}/$< | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[3] =~ /hsa-/g){ my @split_prom_info = split(/;/, $$line[3]); my @miRs = split(/,/, $$split_prom_info[2]); foreach my $$miR (@miRs){ print $$line[0]."\t".$$line[1]."\t".$$line[2]."\t".$$miR;} };' > ${TABLES_PROMOTER_DIR}/miR_F5_prom-500nt+500nt.bed;

_miR_F5_prom-300nt+100nt.bed: miR_F5_TSS.bed $(hg19size)
	bedtools slop -l 300 -r 100 -s -g $(hg19size) -i ${TABLES_OTHER_DIR}/$< | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[3] =~ /hsa-/g){ my @split_prom_info = split(/;/, $$line[3]); my @miRs = split(/,/, $$split_prom_info[2]); foreach my $$miR (@miRs){ print $$line[0]."\t".$$line[1]."\t".$$line[2]."\t".$$miR;} };' > ${TABLES_PROMOTER_DIR}/miR_F5_prom-300nt+100nt.bed;

################################################################
## Find mutations overlapping the miR promoters
# BASIS_mut_in_miR_promoters:
	@echo "Find mutations overlapping miR promoters"
	@echo
	@echo "Finding mutations overlapping miR promoters (+1kb, -1kb)"
	@echo
	${MAKE} _mutations_in_miR_promoters-1kb+1kb
	@echo
	@echo "Finding mutations overlapping miR promoters (+500bp, -500bp)"
	@echo
	${MAKE} _mutations_in_miR_promoters-500nt+500nt
	@echo
	@echo "Finding mutations overlapping miR promoters (+300bp, -100bp)"
	@echo
	${MAKE}  _mutations_in_miR_promoters-300nt+100nt


_mutations_in_miR_promoters-1kb+1kb:
	@echo
	${MAKE} _BASIS_subst_in_prom-1kb+1kb.tsv
	@echo
	${MAKE} _BASIS_indels_in_prom-1kb+1kb.tsv
	@echo
	cat ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-1kb+1kb.tsv ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-1kb+1kb.tsv > ${TABLES_PROMOTER_DIR}/BASIS_mut_in_prom-1kb+1kb.tsv;

_BASIS_subst_in_prom-1kb+1kb.tsv: ${TABLES_PROMOTER_DIR}/miR_F5_prom-1kb+1kb.bed Caveman_560_20Nov14_clean.VCF
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Caveman_560_20Nov14_clean.VCF -b ${TABLES_PROMOTER_DIR}/miR_F5_prom-1kb+1kb.bed > ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-1kb+1kb.tsv;

_BASIS_indels_in_prom-1kb+1kb.tsv: ${TABLES_PROMOTER_DIR}/miR_F5_prom-1kb+1kb.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b ${TABLES_PROMOTER_DIR}/miR_F5_prom-1kb+1kb.bed > ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-1kb+1kb.tsv;


_mutations_in_miR_promoters-500nt+500nt:
	@echo
	${MAKE} _BASIS_subst_in_prom-500nt+500nt.tsv
	@echo
	${MAKE} _BASIS_indels_in_prom-500nt+500nt.tsv
	@echo
	${MAKE} _BASIS_mut_in_prom-500nt+500nt.tsv

_BASIS_subst_in_prom-500nt+500nt.tsv: ${TABLES_PROMOTER_DIR}/miR_F5_prom-500nt+500nt.bed Caveman_560_20Nov14_clean.VCF
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Caveman_560_20Nov14_clean.VCF -b ${TABLES_PROMOTER_DIR}/miR_F5_prom-500nt+500nt.bed > ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-500nt+500nt.tsv;

_BASIS_indels_in_prom-500nt+500nt.tsv: ${TABLES_PROMOTER_DIR}/miR_F5_prom-500nt+500nt.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b ${TABLES_PROMOTER_DIR}/miR_F5_prom-500nt+500nt.bed > ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-500nt+500nt.tsv;

_BASIS_mut_in_prom-500nt+500nt.tsv: ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-500nt+500nt.tsv ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-500nt+500nt.tsv 
	cat ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-500nt+500nt.tsv ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-500nt+500nt.tsv > ${TABLES_PROMOTER_DIR}/BASIS_mut_in_prom-500nt+500nt.tsv;


_mutations_in_miR_promoters-300nt+100nt:
	@echo
	${MAKE} _BASIS_subst_in_prom-300nt+100nt.tsv
	@echo
	${MAKE} _BASIS_indels_in_prom-300nt+100nt.tsv
	@echo
	${MAKE} _BASIS_mut_in_prom-300nt+100nt.tsv

_BASIS_subst_in_prom-300nt+100nt.tsv: ${TABLES_PROMOTER_DIR}/miR_F5_prom-300nt+100nt.bed Caveman_560_20Nov14_clean.VCF
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Caveman_560_20Nov14_clean.VCF -b ${TABLES_PROMOTER_DIR}/miR_F5_prom-300nt+100nt.bed > ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-300nt+100nt.tsv;

_BASIS_indels_in_prom-300nt+100nt.tsv: ${TABLES_PROMOTER_DIR}/miR_F5_prom-300nt+100nt.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF
	bedtools intersect -wo -a ${TABLES_OTHER_DIR}/Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b ${TABLES_PROMOTER_DIR}/miR_F5_prom-300nt+100nt.bed > ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-300nt+100nt.tsv;

_BASIS_mut_in_prom-300nt+100nt.tsv: ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-300nt+100nt.tsv ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-300nt+100nt.tsv 
	cat ${TABLES_OTHER_DIR}/BASIS_subst_in_prom-300nt+100nt.tsv ${TABLES_OTHER_DIR}/BASIS_indels_in_prom-300nt+100nt.tsv > ${TABLES_PROMOTER_DIR}/BASIS_mut_in_prom-300nt+100nt.tsv;


################################################################
## Create an association table of each miR to its corresponding pre-miR
## Column 1: miR name 
## Column 2: pre-miR name
hsa_mir_to_premir.tsv: $(mirbase)/hsav20.gff3
	sed '/^#/d;s/;/\t/g;s/=/\t/g' $< | \
	awk '/primary_transcript/{primary[$$10] = $$14} \
		!/primary_transcript/{print $$14"\t"primary[$$16]}' | sort -k 2b,2 > ${TABLES_OTHER_DIR}/$@;


################################################################
## Create an association table of each pre-miR to its associated promoter
## Promoter association by proximity - According to FANTOM5
## Column 1: pre-miR name
## Column 2: promoter
premir_to_tss.tsv: miR_F5_TSS.bed
	sed 's/;/\t/g' ${TABLES_OTHER_DIR}/$< | \
			awk '{\
			split($$6,premirs,",");\
			for(indx in premirs){\
			print premirs[indx]"\t"$$4}\
			}' > ${TABLES_OTHER_DIR}/$@;

################################################################
## Create mutation tables for xseq (primiR, premiR, promoters)
## Each table should have at least 3 columns:
## 1) Gene name (pri-miRNA)
## 2) Sample (Patient) ID
## 3) Mutation Type
xseq_mut_tables:
	@echo
	@echo "Mutation table for pri-miRs"
	mkdir -p ${TABLES_PRIMIR_DIR}/xseq
	${MAKE} _xseq_primiR_mut_table.tsv
	@echo
	@echo "Mutation table for pre-miRs"
	mkdir -p ${TABLES_PREMIR_DIR}/xseq
	${MAKE} _xseq_premiR_mut_table.tsv
	@echo
	@echo "Mutation table for miR promoters (-1kb, +1kb)"
	mkdir -p ${TABLES_PROMOTER_DIR}/xseq
	${MAKE} _xseq_promoters_-1kb+1kb_mut_table.tsv
	@echo
	@echo "Mutation table for miR promoters (-500nt, +500nt)"
	mkdir -p ${TABLES_PROMOTER_DIR}/xseq
	${MAKE} _xseq_promoters_-500nt+500nt_mut_table.tsv
	@echo
	@echo "Mutation table for miR promoters (-300nt, +100nt)"
	mkdir -p ${TABLES_PROMOTER_DIR}/xseq
	${MAKE} _xseq_promoters_-300nt+100nt_mut_table.tsv


_xseq_primiR_mut_table.tsv: ${TABLES_PRIMIR_DIR}/BASIS_mut_in_primiR.tsv
	cut -f3,14,10 $< | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[0] =~ /^\D+(\d+)\D+$$/){$$parsed_ID = $$1; $$parsed_ID =~ s/\s+//gi;}; print $$line[2]."\t".$$parsed_ID."\t".$$line[1];' > ${TABLES_PRIMIR_DIR}/xseq/xseq_primiR_mut_table.tsv;

_xseq_premiR_mut_table.tsv: ${TABLES_PREMIR_DIR}/BASIS_mut_in_premiR.tsv
	cut -f3,10,16 $< | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[0] =~ /^\D+(\d+)\D+$$/){$$parsed_ID = $$1; $$parsed_ID =~ s/\s+//gi;}; print $$line[2]."\t".$$parsed_ID."\t".$$line[1];' > ${TABLES_PREMIR_DIR}/xseq/xseq_premiR_mut_table.tsv;

_xseq_promoters_-1kb+1kb_mut_table.tsv: ${TABLES_PROMOTER_DIR}/BASIS_mut_in_prom-1kb+1kb.tsv
	cut -f3,14,10 results/parsed_tables/promoters_data/BASIS_mut_in_prom-1kb+1kb.tsv | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[0] =~ /^\D+(\d+)\D+$$/){$$parsed_ID = $$1; $$parsed_ID =~ s/\s+//gi;}; my @matches = ($$line[2] =~ /hsa-/g); my $$nb_match = @matches; my @split_prom_info = split(/;/, $$line[2]); my @miRs = split(/,/, $$split_prom_info[2]); foreach my $$miR (@miRs){ print $$miR."\t".$$parsed_ID."\t".$$line[1];}' > ${TABLES_PROMOTER_DIR}/xseq/xseq_promoters_-1kb+1kb_mut_table.tsv;

_xseq_promoters_-500nt+500nt_mut_table.tsv: ${TABLES_PROMOTER_DIR}/BASIS_mut_in_prom-500nt+500nt.tsv
	cut -f3,14,10 results/parsed_tables/promoters_data/BASIS_mut_in_prom-1kb+1kb.tsv | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[0] =~ /^\D+(\d+)\D+$$/){$$parsed_ID = $$1; $$parsed_ID =~ s/\s+//gi;}; my @matches = ($$line[2] =~ /hsa-/g); my $$nb_match = @matches; my @split_prom_info = split(/;/, $$line[2]); my @miRs = split(/,/, $$split_prom_info[2]); foreach my $$miR (@miRs){ print $$miR."\t".$$parsed_ID."\t".$$line[1];}' > ${TABLES_PROMOTER_DIR}/xseq/xseq_promoters_-500nt+500nt_mut_table.tsv;

_xseq_promoters_-300nt+100nt_mut_table.tsv: ${TABLES_PROMOTER_DIR}/BASIS_mut_in_prom-300nt+100nt.tsv
	cut -f3,14,10 results/parsed_tables/promoters_data/BASIS_mut_in_prom-1kb+1kb.tsv | perl -lne 'chomp; @line = split(/\t/, $$_); if($$line[0] =~ /^\D+(\d+)\D+$$/){$$parsed_ID = $$1; $$parsed_ID =~ s/\s+//gi;}; my @matches = ($$line[2] =~ /hsa-/g); my $$nb_match = @matches; my @split_prom_info = split(/;/, $$line[2]); my @miRs = split(/,/, $$split_prom_info[2]); foreach my $$miR (@miRs){ print $$miR."\t".$$parsed_ID."\t".$$line[1];}' > ${TABLES_PROMOTER_DIR}/xseq/xseq_promoters_-300nt+100nt_mut_table.tsv;


################################################################
## Run xseq o different data:
## pri-miRs, pre-miRs, miR promoters

_xseq_command:
	cat ${BIN_R}/Running-xseq-for-miRs.R | ${R} --slave --no-save --no-restore --no-environ --args " \
	results.dir = '${XSEQ_DIR_FOLDER}' ; \
	mut.miR.tab.file = '${XSEQ_MUT_TAB}' ; \
	expr.miR.tab.file = '${XSEQ_EXPR_TAB}' ; \
	miR.premiR.tab.file = '${XSEQ_MIR_PREMIR}' ; \
	feature.bed.file = '${XSEQ_FEATURE_BED}' ; \
	prefix.name = '${XSEQ_PREFIX}' ";


run_xseq:
	@echo
	@echo "Running xseq"
	@echo
	@echo "Running xseq for pri-miRNA data"
	${MAKE} _xseq_command XSEQ_DIR_FOLDER=results/xseq \
			      XSEQ_MUT_TAB=${TABLES_PRIMIR_DIR}/xseq/xseq_primiR_mut_table.tsv \
			      XSEQ_EXPR_TAB=${MIR_EXPR_TAB} \
			      XSEQ_MIR_PREMIR=${TABLES_OTHER_DIR}/hsa_mir_to_premir.tsv \
			      XSEQ_FEATURE_BED=${TABLES_PRIMIR_DIR}/primiRs_F5.bed \
			      XSEQ_PREFIX=primiRNA
	@echo
	@echo "Running xseq for pre-miRNA data"
	${MAKE} _xseq_command XSEQ_DIR_FOLDER=results/xseq \
			      XSEQ_MUT_TAB=${TABLES_PREMIR_DIR}/xseq/xseq_premiR_mut_table.tsv \
			      XSEQ_EXPR_TAB=${MIR_EXPR_TAB} \
			      XSEQ_MIR_PREMIR=${TABLES_OTHER_DIR}/hsa_mir_to_premir.tsv \
			      XSEQ_FEATURE_BED=${TABLES_PREMIR_DIR}/premiRs_F5.bed \
			      XSEQ_PREFIX=premiRNA
	@echo
	@echo "Running xseq for promoter miRNA (+1kb, -1kb)"
	${MAKE} _xseq_command XSEQ_DIR_FOLDER=results/xseq \
			      XSEQ_MUT_TAB=${TABLES_PROMOTER_DIR}/xseq/xseq_promoters_-1kb+1kb_mut_table.tsv \
			      XSEQ_EXPR_TAB=${MIR_EXPR_TAB} \
			      XSEQ_MIR_PREMIR=${TABLES_OTHER_DIR}/hsa_mir_to_premir.tsv \
			      XSEQ_FEATURE_BED=${TABLES_PROMOTER_DIR}/miR_F5_prom-1kb+1kb.bed \
			      XSEQ_PREFIX=prom-1kb+1kb
	@echo
	@echo "Running xseq for promoter miRNA (+500nt, -500nt)"
	${MAKE} _xseq_command XSEQ_DIR_FOLDER=results/xseq \
			      XSEQ_MUT_TAB=${TABLES_PROMOTER_DIR}/xseq/xseq_promoters_-500nt+500nt_mut_table.tsv \
			      XSEQ_EXPR_TAB=${MIR_EXPR_TAB} \
			      XSEQ_MIR_PREMIR=${TABLES_OTHER_DIR}/hsa_mir_to_premir.tsv \
			      XSEQ_FEATURE_BED=${TABLES_PROMOTER_DIR}/miR_F5_prom-500nt+500nt.bed \
			      XSEQ_PREFIX=prom-500nt+500nt




################################################################
## Temporarily disabled 
################################################################

# BASIS_subst_in_prom-2kb+2kb_TFBS.tsv: miR_F5_prom-2kb+2kb.TFBS.bed Caveman_560_20Nov14_clean.VCF
# 	bedtools intersect -wo -a Caveman_560_20Nov14_clean.VCF -b $< > $@;

# BASIS_indels_in_prom-2kb+2kb_TFBS.tsv: miR_F5_prom-2kb+2kb.TFBS.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF
# 	bedtools intersect -wo -a Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b $< > $@;

# BASIS_mut_in_prom-2kb+2kb_TFBS.tsv: BASIS_subst_in_prom-2kb+2kb_TFBS.tsv BASIS_indels_in_prom-2kb+2kb_TFBS.tsv 
# 	cat $^ > $@;


# miR_F5_prom-1kb+1kb.TFBS.bed: miR_F5_TSS.bed $(DATA)/20161003_MANTA_TFBS/all_sorted_merged.bed $(hg19size)
# 	awk '{print "chr"$$0}' $(DATA)/20161003_MANTA_TFBS/all_sorted_merged.bed > tfbs.$$PPID;
# 	bedtools slop -b 1000 -g $(hg19size) -i $< | bedtools intersect -wo -a tfbs.$$PPID -b stdin \
# 	| awk 'BEGIN{OFS="\t"} {print $$1, $$2, $$3, $$7, $$8, $$9}' > $@;
# 	rm tfbs.$$PPID;

# miR_F5_prom-500+500.TFBS.bed: miR_F5_TSS.bed $(DATA)/20161003_MANTA_TFBS/all_sorted_merged.bed $(hg19size)
# 	awk '{print "chr"$$0}' $(DATA)/20161003_MANTA_TFBS/all_sorted_merged.bed > tfbs.$$PPID;
# 	bedtools slop -b 500 -g $(hg19size) -i $< | bedtools intersect -wo -a tfbs.$$PPID -b stdin \
# 	| awk 'BEGIN{OFS="\t"} {print $$1, $$2, $$3, $$7, $$8, $$9}' > $@;
# 	rm tfbs.$$PPID;

# miR_F5_prom-300+100.TFBS.bed: miR_F5_TSS.bed $(DATA)/20161003_MANTA_TFBS/all_sorted_merged.bed $(hg19size)
# 	awk '{print "chr"$$0}' $(DATA)/20161003_MANTA_TFBS/all_sorted_merged.bed > tfbs.$$PPID;
# 	bedtools slop -l 300 -r 100 -s -g $(hg19size) -i $< | bedtools intersect -wo -a tfbs.$$PPID -b stdin \
# 	| awk 'BEGIN{OFS="\t"} {print $$1, $$2, $$3, $$7, $$8, $$9}' > $@;
# 	rm tfbs.$$PPID;

# BASIS_subst_in_prom-500+500_TFBS.tsv: miR_F5_prom-500+500.TFBS.bed Caveman_560_20Nov14_clean.VCF
# 	bedtools intersect -wo -a Caveman_560_20Nov14_clean.VCF -b $< > $@;

# BASIS_indels_in_prom-500+500_TFBS.tsv: miR_F5_prom-500+500.TFBS.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF
# 	bedtools intersect -wo -a Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b $< > $@;

# BASIS_mut_in_prom-500+500_TFBS.tsv: BASIS_subst_in_prom-500+500_TFBS.tsv BASIS_indels_in_prom-500+500_TFBS.tsv 
# 	cat $^ > $@;

# BASIS_subst_in_prom-300+100_TFBS.tsv: miR_F5_prom-300+100.TFBS.bed Caveman_560_20Nov14_clean.VCF
# 	bedtools intersect -wo -a Caveman_560_20Nov14_clean.VCF -b $< > $@;

# BASIS_indels_in_prom-300+100_TFBS.tsv: miR_F5_prom-300+100.TFBS.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF
# 	bedtools intersect -wo -a Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF -b $< > $@;

# BASIS_mut_in_prom-300+100_TFBS.tsv: BASIS_subst_in_prom-300+100_TFBS.tsv BASIS_indels_in_prom-300+100_TFBS.tsv 
# 	cat $^ > $@;

# BASIS_mut_in_prom-500+500.tsv: BASIS_indels_in_prom-500+500.tsv BASIS_subst_in_prom-500+500.tsv
# 	cat $^ > $@;

# BASIS_subst_in_prom-300+100.tsv: miR_F5_TSS.bed Caveman_560_20Nov14_clean.VCF $(hg19size)
# 	bedtools slop -i $< -g $(hg19size) -l 300 -r 100 -s | \
# 		bedtools intersect -wo -a Caveman_560_20Nov14_clean.VCF -b stdin > $@;

# BASIS_indels_in_prom-300+100.tsv: miR_F5_TSS.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF $(hg19size)
# 	bedtools slop -i $< -g $(hg19size) -s -l 300 -r 100 | \
# 		bedtools intersect -wo \
# 		-a Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF \
# 		-b stdin > $@;

# BASIS_mut_in_prom-300+100.tsv: BASIS_indels_in_prom-300+100.tsv BASIS_subst_in_prom-300+100.tsv
# 	cat $^ > $@;

# BASIS_subst_in_prom-2kb+2kb.tsv: miR_F5_TSS.bed Caveman_560_20Nov14_clean.VCF $(hg19size)
# 	bedtools slop -i $< -g $(hg19size) -b 2000 | \
# 		bedtools intersect -wo -a Caveman_560_20Nov14_clean.VCF -b stdin > $@;

# BASIS_indels_in_prom-2kb+2kb.tsv: miR_F5_TSS.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF $(hg19size)
# 	bedtools slop -i $< -g $(hg19size) -b 2000 | \
# 		bedtools intersect -wo \
# 		-a Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF \
# 		-b stdin > $@;

# BASIS_mut_in_prom-2kb+2kb.tsv: BASIS_indels_in_prom-2kb+2kb.tsv BASIS_subst_in_prom-2kb+2kb.tsv
# 	cat $^ > $@;

# BASIS_subst_in_prom-500+500.tsv: miR_F5_TSS.bed Caveman_560_20Nov14_clean.VCF $(hg19size)
# 	bedtools slop -i $< -g $(hg19size) -b 2000 | \
# 		bedtools intersect -wo -a Caveman_560_20Nov14_clean.VCF -b stdin > $@;

# BASIS_indels_in_prom-500+500.tsv: miR_F5_TSS.bed Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF $(hg19size)
# 	bedtools slop -i $< -g $(hg19size) -b 500 | \
# 		bedtools intersect -wo \
# 		-a Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF \
# 		-b stdin > $@;




# # Mutations per position
# less BASIS_mut_in_primiR.tsv | cut -f2 | uniq -c | sort -h

# # Mutations per ID
# less BASIS_mut_in_primiR.tsv | cut -f3 | uniq -c | sort -h

# bedtools intersect -wo -a Caveman_560_20Nov14_clean.VCF -b primiRs_F5.bed > BASIS_subst_in_primiR.tsv;

# https://genome-euro.ucsc.edu/cgi-bin/hgTracks?db=hg38&lastVirtModeType=default&lastVirtModeExtraState=&virtModeType=default&virtMode=0&nonVirtPosition=&position=chr14%3A100714566-101382815&hgsid=224930197_ABDXWLUevtHpZHUWqw1Swab0IGTt

# CHECK
# more BASIS_mut_in_primiR.tsv | grep 101339097 | wc -l
# more Caveman_560_20Nov14_clean.VCF | grep 101339097 | wc -l
# more Pindel_560_20Nov14_noFemaleY_SUMMS_REP.txt.1000gFiltered.VCF | grep 101339097 | wc -l

# more BASIS_mut_in_primiR.tsv | cut -f1,2,3,4,9,13 | less

# more BASIS_mut_in_primiR.tsv | cut -f3 | uniq | sort -h | perl -lne 'chomp; @line = split("\t", $_); $line[0] =~ s/\D+//g; $parsed_ID = $line[0]; print $parsed_ID;' | uniq | wc -l
